# About DINFRA

## Decentralized Infrastructure with Substrate

DINFRA, decentralized infrastructure, is substrate orchestrated infrastructure, a decentralized alternative to public clouds.

This project is in the process of Web3 Foundation Grants. See [Grant Application](https://github.com/w3f/Grants-Program/blob/master/applications/DINFRA.md) for details 

DINFRA is infrastructure and language agnostic. Infrastructure providers can integrate the infrastructure of their choice using the language of their choice.

# Components

Introduction to the DINFRA Archicture.

* [DINFRA Parachain](https://gitlab.com/dinfra/substrate-parachain). The DINFRA Parachain uses a Substrate Blockchain to keep track of deployment contract lifecycles. It is linked to this git repo as submodule. Infrastructrure providers are encoraged to run a Collator node as part of their DINFRA deployment, although it is not strictly necessary.  

* [OGate](./packages/ogate): The DINFRA OpenAPI Gateway. OGate is a DINFRA Collator sidecar that serves as the Interface between the DINFRA Parachain and the Chain Reactors. OGate Translates OpenAPI REST Requests from the Chain Reactors to DINFRA RPC API Calls and Queries. 
* [ACS-Reactor](./packages/acs-reactor): Chain Reactor Reference Imlpementration. It uses the DINFRA API, an OpenAPI Client used to connect Chain Reactors to DINFRA via OGate. `acs-reactor` is a python project, however it comes with a `package.json` to provide script entrypoints.

The Components are docker-friendly and can be configured via environment variables. Ogate and Reactors are collectively referred as "sidecars".

# Testing Guide

In order to test DINFRA, your environment needs to have the following capabilities:

- Node 18
- Python 3.8, usually provided by your Linux distribution
- [Rust](https://docs.substrate.io/install/) 
- Docker and Docker Compose

## Unit Testing

DINFRA is a workspace project with multiple packages, however, testing can be triggered from the root of the project.
Yarn commands are delegated to the child packages. 

In order to run the unit tests clone the project and do:

- ```yarn install``` to install all node packages
- ```yarn test:all``` to run all unit tests, including sidecar and parachain tests.

Unit and Integration tests are also run on every commit. The results can be monitored in [gitlab pipelines](https://gitlab.com/dinfra/dinfra/-/pipelines).

## Test Run of the System

All modules deliver docker containers, and a docker-compopose, with multiple profiles, allow to run all or certain
components depending on the desired activity

### Setting up the environment 

1. Build the containers for all components with ```yarn docker:build```
2. You can run all the components with ``yarn docker:testdeploy``

After triggering ```testdeploy```, you will notice that the Apache Cloudstack starts initializing, which may take some time,
once concluded playgrounds become available.

The standard ```testdeploy``` run will start simulating deployment lifecycles. You can log on to the Apache Cloudstack Simulator
to follow the creation, update, and destruction of Cloud Computing objects accordingly. Project, Networks, Instances, Accounts,
Domains will be created and associated with DINFRA Ids. Enablement of Projects will be required to see the instances created.

## Available Playgrounds

You can access the following playgrouds / UIs to monitor and test your development:

1. [Apache Cloudstack Simulator](http://localhost:8888): Simulates a Cloud Platform. Infrastructure can be created, such as Virtual Machines or Networks, but they are only simulated in the management console. Use `admin/password` as UI credentials. Development keys are generated during initialization and become available [in this url](http://localhost:8888/admin.json).
2. [OGate API UI](http://localhost:7300/dinfra): Used to test the DINFRA Client API. You will need to authorize with a valid token. When running in Development mode, OGate will log Development Tokens that do not expire for 3 development accounts.

## Substrate Test Network

A substrate test network is also started by ```testdeploy```. The whole network runs inside a ```zombienet``` container
thanks to the [zombienet utility image](./util/zombienet).

Deployment data not required for consensus is stored off-chain in ipfs, an ipfs network is also started. 

The container will only log the output of ```zombienet``` itself. You can however use:

- `yarn docker:zombietail` or `yarn docker:zombietailw` to display a multitail of the node log files all merged or in multiple windows.

And regarding Frontends:

- Use [Alice](https://polkadot.js.org/apps/?rpc=ws://127.0.0.1:37731#/explorer) or [Bob](https://polkadot.js.org/apps/?rpc=ws://127.0.0.1:37732#/explorer) to access RelayChain Nodes Frontend
- Use [Collator01](https://polkadot.js.org/apps/?rpc=ws://127.0.0.1:37741#/explorer) to access the collator node Frontend
- Use [IPFS Webui](http://127.0.0.1:5001/webui) to access the IPFS web UI. Explore or update test data CIDs by running: `yarn docker:ipfs:testdata`. The data is made available on container start, and a content gateway on port `8080` is also available for the rest of components. 

Parachain integration tests can be run, based on, zombienet `zndsl`, using the command:

- `yarn docker::zombietest`

Check `package.json` for additional script entrypoints.

### Sample test script

When test-running DINFRA for the first time, we recommend the following manual test script.

- Open the Collator UI and Apache Cloudstack UI (see Playgrounds / frontends above)
- Log into Cloudstack UI
- With the Test Deployment, ALICE has been configured as Infrastructure Provider.
- On Collator/UI Development extrinsics do:
- Any user, ALICE for example, must create a Deployment Type. Use the `dinfraProvider.createDeploymentType`extrinsic to create the deployment type 1, with one of the available CIDs (see IPFS test data above) for example `QmQeGpsWuvsrN2a7VGesPpk6kAm4YKafiUrwDtRxgyppR6` and description SimpleVM
- Now ALICE must create an Offering, to let the network know that she can offer SimpleVM infrastructure.
- ALICE can now use `dinfraProvider.createOffering` for the Deployment Type 1 (just created) with capacity type measured in Number of Deployments, that can hold 5 deployments, and rate type as FixedPerBlock and rate of 1 000 000 000 (per block). The offering is Open from start.
- Now ALICE is fully setup as infrastructure provider. Has configured a deployment type, with capacity and cost configured.
- The rest of test users can now consume infrastructure from ALICE using the `dinfraSubscription` pallet.
- BOB, for example, can create a Subscription with `dinfraSubscription.createSubscription`. The particular configuration to request, or deployment descriptor will be one of the available CIDs (see IPFS test data above) for example `QmUNH3tMN1rbgmvTCMH6ZbgofkHokAoj688uwzkEzgQiRP` , which conforms with Deployment Type 1 created before, and must be provided,  there isn't a Provider Selector created yet, so BOB will select an explicit provider to serve the subscription, and selects ALICE. BOB must also reserve funds for the subscription, for example: 25 000 000 000. This will cover the deposit and allow the infrastructure to run for 15 blocks, at the rate above.
- DINFRA needs to award the subscription to a provider, if it is possible, taking into account preference, load, funds, etc.
- The Award process will be scheduled a few blocks ahead and ALICE will be awarded the deployment.
- OGate will start receiving events and forwarding them to the Reactor that will start to orchestrate infrastructure.
- In Cloudstack UI, New Objects (Projects, Networks, VMs) will start to show up (remember to enable projects in Compute/Instances tab)
- DINFRA has also scheduled the termination of the infrastructure at the point in which funds are projected to run out.
- Some 15 blocks in the future, the wind down of the infrastructure will take place.
- Subcriptions and Subscribers (which are equivalent 1 to 1) must delete the subscription in order to get the deposit back. Subscriptions are not automatically deleted on termination, this allows subscribers and providers to inspect, for example, what was the termination reason.
- ALICE was setup with 5 deployments as capacity. 
- Other test users can setup similar subscriptions at the same time.

You can now create alternative subscription lifecycles. Check the unit tests of the subscription pallet to see examples of alternative lifecycles.

Be aware that not all available pallet options may be implemented, check pallet documentation for details.

You can also crate test data which is stored [here](./descriptors/test).

### Developing locally

In order to develop locally you can use:

1. Build all components from root with ``yarn build``
2. You can run packages locally, i.e in your IDE, and run all other dependencies with ```yarn docker:dev```. The OGate and Reactor servers
   will not be started. 
3. Additionally ``yarn docker:clean`` will clean up all containers and data volumes, if any.

### Developing a Chain Reactor

For detailed instructions check the [chain reactor documentation/tutorial](./packages/acs-reactor ).

### Technology Stack

- [Substrate](https://substrate.io/): The Blockchain Framework for a Multichain Future
- [NestJs](https://nestjs.com/): A progressive Node.js framework for building efficient, reliable and scalable server-side applications with NodeJS.

Reactor Reference implementation
- [Apache CloudStack](https://cloudstack.apache.org/): Open Source Cloud Computing™
- [Ansible](https://docs.ansible.com/ansible/latest/index.html): Ansible is an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates.

# License and Copyright

Polkawatch is Open Source, Apache License Version 2.0.

©2023 [Valletech AB](https://valletech.eu), Authors and Contributors.

![w3f foundation grants program](https://blog.polkawatch.app/content/images/2023/10/web3-foundation_grants_badge_black.svg)
