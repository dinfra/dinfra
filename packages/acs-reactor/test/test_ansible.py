# Copyright 2023 Valletech AB authors & contributors

import unittest
import os
import sys
import json
import ansible_runner
import pytest as pytest
import requests


class AnsibleTestCase(unittest.TestCase):
    # We are testing ansible_runner which is a programmatic interface for ansible with multiple features.
    def test_playbook_execution(self):
        r = ansible_runner.interface.run(
            private_data_dir=data_dir('ansible_test'),
            playbook="test.yml",
        )
        self.assertEqual(r.status, "successful")
        self.assertEqual(r.rc, 0)

    def test_playbook_execution_with_json_param(self):
        # Where we test passing complex information we will receive to the playbook
        deployment_descriptor_str = '''
            {
               "ipfs_service": {
                  "pin_set": [
                     {
                        "name": "MyDAPPUI",
                        "CID": "QmZbBe6qKdHufXnVYEKap6vae4DgfAhRwrzt69MAdqHK9k"
                     },
                     {
                        "name": "MyAPPWebsite",
                        "CID": "QmateDTR5eabDoeEemMB2L6mFmEAZGB2eyNPf3gZPWHgcy"
                     },
                     {
                        "name": "MyAPPDocumentation",
                        "CID": "QmPAycWnksXTKaz4DZYjC132EMEhH5GyUQoVfCezZCzyjP"
                     }
                  ],
                  "region_set": [
                     "CN",
                     "DE",
                     "SV"
                  ]
               }
            }
        '''
        deployment_descriptor = json.loads(deployment_descriptor_str)

        # We can pass extravars and they will be added to the
        # extravars already defined in a file
        # works fine as dictionary
        r = ansible_runner.interface.run(
            private_data_dir=data_dir('ansible_test'),
            playbook="test_descriptor.yml",
            extravars={
                "deployment_descriptor": deployment_descriptor
            },
        )
        self.assertEqual(r.status, "successful")
        self.assertEqual(r.rc, 0)

    @pytest.mark.manual
    def test_playbook_execution_with_acs_module(self):
        # Here we are testing calling the actual ACS simulator via Ansible
        # this manual test requires the simulator to be running
        response = requests.get("http://localhost:8888/admin.json")
        response.raise_for_status()
        json_data = response.json()

        # Ansible will spawn a new interpreter to run the playbook
        # Since we are using a virtual env, it won't pick the cs module, unless I pass our interpreter in
        r = ansible_runner.interface.run(
            private_data_dir=data_dir('ansible_test'),
            playbook="test_acs.yml",
            extravars={
                "ansible_python_interpreter": sys.executable
            },
            envvars={
                "CLOUDSTACK_ENDPOINT": "http://localhost:8888/client/api",
                "CLOUDSTACK_KEY": json_data['apikey'],
                "CLOUDSTACK_SECRET": json_data['secretkey'],
            }
        )
        self.assertEqual(r.status, "successful")
        self.assertEqual(r.rc, 0)


def data_dir(d):
    # Works out datadir relative to the test, making IDEs happy
    test_dir = os.path.abspath(__file__)
    data_dir = os.path.join(test_dir, '..', d)
    return os.path.normpath(data_dir)


if __name__ == "__main__":
    unittest.main()
