# Copyright 2023 Valletech AB authors & contributors
import time
import unittest
import jwt
from jwt import InvalidSignatureError, ExpiredSignatureError
import datetime
from datetime import timezone


class JWTTestCase(unittest.TestCase):
    def test_token_generation_verification(self):
        payload = {
            'providerAccountId': 1,
        }
        my_secret = 'my super secret'
        token = jwt.encode(
            payload=payload,
            key=my_secret
        )
        # Lets verify the token
        decoded = jwt.decode(token, my_secret, algorithms='HS256')
        self.assertEqual(decoded['providerAccountId'], payload['providerAccountId'])
        # We can decode without verification
        decoded = jwt.decode(token, options={'verify_signature': False})
        self.assertEqual(decoded['providerAccountId'], payload['providerAccountId'])
        # We can also read headers without verification
        headers = jwt.get_unverified_header(token)
        self.assertEqual(headers['alg'], 'HS256')

    def test_token_generation_fails_with_wrong_key(self):
        payload = {
            'providerAccountId': 1,
        }
        my_secret = 'my super secret'
        token = jwt.encode(
            payload=payload,
            key=my_secret
        )
        # Lets verify the token won't be decoded with the wrong key
        with self.assertRaises(InvalidSignatureError):
            jwt.decode(token, 'my wrong key', algorithms='HS256')

    def test_token_generation_with_expiry_date(self):
        payload = {
            'providerAccountId': 1,
            'exp': datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(seconds=2)
        }
        my_secret = 'my super secret'
        token = jwt.encode(
            payload=payload,
            key=my_secret
        )
        # it decodes OK
        jwt.decode(token, my_secret, algorithms='HS256')
        # Wait a few seconds, and retry
        time.sleep(3)
        with self.assertRaises(ExpiredSignatureError):
            jwt.decode(token, my_secret, algorithms='HS256')


if __name__ == '__main__':
    unittest.main()
