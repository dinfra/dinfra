# Copyright 2023 Valletech AB authors & contributors

# end-to-end API tests require OGate running in RI Testing Mode.

import unittest
import pytest

import dinfra_client
from dinfra_client.api import server_api

# API Client Configuration
configuration = dinfra_client.Configuration(
    host="http://localhost:7300"
)
client = dinfra_client.ApiClient(configuration)

# Create an instance of the API class
api = server_api.ServerApi(client)


@pytest.mark.manual
class ApiVersionTest(unittest.TestCase):

    def test_api_version(self):

        # The version endpoint should return a semantic version
        # TODO: Horrible generated method name
        result = api.get_version()
        self.assertRegex(result.version, r'^\d+\.\d+\.\d+$')

    def test_receive_event(self):
        event = api.get_event()
        self.assertEqual(event.type, 'NoEvent')


if __name__ == '__main__':
    unittest.main()
