# Copyright 2023 Valletech AB authors & contributors

import os
import sys
import time
import datetime
import requests
from datetime import timezone

from loguru import logger
import jwt

import json
import ansible_runner

import dinfra_client
from dinfra_client.api import server_api
from dinfra_client.exceptions import OpenApiException, ApiException

from urllib3.exceptions import MaxRetryError

# General Docker Friendly Configuration

REACTOR_OGATE_URL = os.getenv('REACTOR_OGATE_URL', 'http://localhost:7300')

# JWT Security Configuration

DEVELOPMENT_SECRET = 'default development token, never use in production'
DEVELOPMENT_ACCOUNT = 'ALICE'
REACTOR_SECRET_TOKEN = os.getenv('REACTOR_SECRET_TOKEN', DEVELOPMENT_SECRET)
REACTOR_ACCOUNT_ID = os.getenv('REACTOR_ACCOUNT_ID', DEVELOPMENT_ACCOUNT)
REACTOR_TOKEN_EXPIRY_S = int(os.getenv('REACTOR_TOKEN_EXPIRY_S', '21'))

# ACS Tokens, only for production, will be fetched in Development mode
REACTOR_ACS_URL = os.getenv('REACTOR_ACS_URL', 'http://localhost:8888')
REACTOR_ACS_API_KEY = os.getenv('REACTOR_ACS_API_KEY', 'DEVELOPMENT')
REACTOR_ACS_SECRET_KEY = os.getenv('REACTOR_ACS_API_KEY', 'DEVELOPMENT')
REACTOR_ACS_RUNNER_MAX_ARTIFACTS = int(os.getenv('REACTOR_ACS_RUNNER_MAX_ARTIFACTS', '10'))

# Quality Assurance Running Mode
REACTOR_QA_MODE = (os.getenv('REACTOR_QA_MODE', 'false').lower() == "true")

# Logger configuration
logger.remove(0)
logger.add(sys.stdout, format="{time:YYYY-MM-DD HH:mm:ss.S} {level} {message}", level="INFO")


def main():
    # The Chain Reactor is basically an Event loop
    running = True
    logger.info("Starting the ACS chain Reactor ...")
    logger.info(f"Running as account {REACTOR_ACCOUNT_ID}")

    # Check if we are in a development session and inform accordingly.
    if REACTOR_SECRET_TOKEN == DEVELOPMENT_SECRET:
        logger.warning("Development secret token detect, DO NOT USE IN PRODUCTION")
        getACSSimulatorKeys()
    if REACTOR_QA_MODE:
        logger.warning("Running in QA mode, will finish after first Simulation")

    while running:
        try:
            api = getApi()
            event = api.get_event()
            subscription_id = None
            subscription = None
            if 'subscriber_account_id' in event:
                subscription_id = event['subscriber_account_id']
                subscription = api.get_subscription(subscription_id)
                logger.info(f"Received DINFRA event {event.type} at subscription {subscription_id}")
            else:
                logger.info(f"Received DINFRA event {event.type}")
            running = handleEvent(event, subscription)
        except ApiException as e:
            if e.status == 491:
                logger.warning("Request cancelled due to previous pending Request")
            elif e.status == 492:
                logger.warning("Request cancelled due to missing content descriptor")
            elif e.status == 493:
                logger.warning("Request cancelled due to non compliant content descriptor")
            else:
                logger.error(f"API exception with status {e.status}: {e.reason}")
            # Slow down, as this error may be happening persistently.
            time.sleep(1)
        except OpenApiException as e:
            logger.error(f"API Exception: {e}")

        except (ConnectionRefusedError, MaxRetryError):
            # This is useful during development, as OGate may come up and down frequently.
            # Also useful during subscription upgrades, or restarts for other reasons
            logger.error("Cannot connect to OGate, will retry in a sec...")
            time.sleep(3)

    logger.info("ACS Chain Reactor finished.")


def handleEvent(event, subscription):
    # handles events. in our case we pass the event to Ansible.
    # always returns True, except when an exit condition is found.
    if event.type == "SimulationFinished":
        if REACTOR_QA_MODE:
            # When we are in QA mode the server finises after oen simulation, which is used as a CI test
            return False
    elif 'subscriber_account_id' in event:
        callAnsiblePlaybook(event, subscription)
    return True


def getApi():
    # We create the dinfra API object which is configured with host
    # And also the access token, which is signed.

    configuration = dinfra_client.Configuration(
        host=REACTOR_OGATE_URL,
        access_token=getAccessToken()
    )
    client = dinfra_client.ApiClient(configuration)
    return server_api.ServerApi(client)


def getAccessToken():
    # We create our access token
    # We claim our AccountID and sign the token valid for a configurable number of seconds
    return jwt.encode(
        payload={
            'providerAccountId': REACTOR_ACCOUNT_ID,
            'exp': datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(seconds=REACTOR_TOKEN_EXPIRY_S)
        },
        key=REACTOR_SECRET_TOKEN
    )


def getACSSimulatorKeys():
    # Normally the ACS development keys are passed via confiruation
    # however, when developing, the simulator makes them available in an endpoint
    global REACTOR_ACS_API_KEY
    global REACTOR_ACS_SECRET_KEY
    try:
        response = requests.get(f"{REACTOR_ACS_URL}/admin.json")
        response.raise_for_status()
        json_data = response.json()
        REACTOR_ACS_API_KEY = json_data['apikey']
        REACTOR_ACS_SECRET_KEY = json_data['secretkey']
        logger.info("ACS Simulator Development API Key loaded")
    except Exception as e:
        logger.error(f"Could not get Keys from ACS Simulator at {REACTOR_ACS_URL}: {e}")
        sys.exit(0)


def callAnsiblePlaybook(event, subscription):
    # We call an ansible playbook
    # Playbooks are named after the deployment type, which makes sense, because a deployment type means different service types.
    # We pass everything we know to Ansible for automation.
    #
    # Deployment types are assumed to have a symbolic name, even thou they will be defined by URL.
    # DINFRA should allow for simbolic name definition
    #
    r = ansible_runner.interface.run(
        private_data_dir=ansible_runner_dir(),
        playbook=f"{subscription.deployment_type.replace(':', '_')}.yml",
        extravars={
            "ansible_python_interpreter": sys.executable,
            "dinfra_event": event.type,
            "dinfra_subscription_state": subscription.state,
            "dinfra_subscriber_account_id": event.subscriber_account_id,
            "dinfra_deployment_descriptor": json.loads(subscription.deployment_descriptor),
        },
        envvars={
            "CLOUDSTACK_ENDPOINT": f"{REACTOR_ACS_URL}/client/api",
            "CLOUDSTACK_KEY": REACTOR_ACS_API_KEY,
            "CLOUDSTACK_SECRET": REACTOR_ACS_SECRET_KEY,
        },
        rotate_artifacts=REACTOR_ACS_RUNNER_MAX_ARTIFACTS,
        cancel_callback=lambda: None

    )
    logger.info(f"Ansible execution finished with state {r.status}")


def ansible_runner_dir():
    # the ansible_runner directory holds additional information and configuration files for ansible
    test_dir = os.path.abspath(__file__)
    data_dir = os.path.join(test_dir, '..', '..', 'ansible_runner')
    return os.path.normpath(data_dir)


if __name__ == "__main__":
    main()
