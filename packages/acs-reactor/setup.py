# Copyright 2023 Valletech AB authors & contributors

from setuptools import setup, find_packages


setup(
    name='ACS Chain Reactor for DINFRA',
    version='0.1',
    author='Your Name',
    description='DINFRA Blockchain Reactor for Apache Cloudstack',
    packages=find_packages(),
    install_requires=[
        # List your dependencies here
    ],
)
