# Reference Implementation Chain Reactor

## Introduction

Infrastructure providers integrate with DINFRA parachain by implementing a Chain Reactor.

This package is a reference implementation of a Chain Reactor based on Ansible for Apache Cloudstack.

Chains reactors can be built for any Infrastructure, using any computer language.

Chain Reactors are very simple to build thanks to architectural decisions made for that purpose.

## How Chain Reactors Work

The Chain Reactors wait for DINFRA events and Reacts to them by orchestrating provider infrastructure.

DINFRA doesn't need to know anything about Infrastructure Platforms, DINFRA just delivers deployment descriptors and events to Infrastructure providers.

DINFRA doesn't need to know anything about deployment descriptors either, that is information that consumer and provider exchange and it is meaningful to them.

Chain Reactors use the high level DINFRA Open API Client to interact with the blockchain.

## Basic flow of a chain reactor

Chain Reactors are basically an event loop that triggers actions depending on the received event. 

like this:

```
    while true
      event = dinfra_api get the next event()
      depending on the event type:
        case event is start infrastructure:
           deployment information = dinfra_api give me additional deployment information 
           with my_provider_code start infrastructure(deployment information) 
        case event is stop infrastructure:
           deployment information = dinfra_api give me additional deployment information
           with my_provider_code start infrastructure(deployment information) 
```

All interactions with DINFRA are managed by the OpenAPI dinfra client, which can be generated in many different languages.

The client API is used to read the event, and gather additional deployment information.

DINFRA does not need to know about the deployment information, but can ensure it conforms the requirements of the provider using JSON Schemas.

JSON Schemas can be used by infrastructure providers to ensure that they receive the information they require to configure the infrastructure. 
You can find out more about [Deployment Descriptors here](../../descriptors). 

## About This Reference Implementation

This reference implementation is an Example of how to build a Chain Reactor.

This example shows how to build a Chain Reactor that uses Apache Cloudstack via Ansible. 

Similar chain reactors could be built for other platforms and with other languages.

Apache CloudStack was chosen because it includes a simulator that is very convenient in order to show the functionality without
having to deploye a full Infrastructure Platform / Cloud Software.

## Test Toolkit

### OGate Simulator

OGate is a sidecar of DINFRA parachain that serves as interface between the DINFRA API Client and the Parachain. 

OGate reduces the complexity of Chain Reactors as they won't need to interact with the Parachain Directly.

OGate also includes a Simulator that trigger events according to a test script provided. 

```yaml
...
steps:
  - timeSeconds: 5
    event:
      type: DeploymentEnquired
      consumerId: consumer1
      deploymentId: deployment1
      deployment_type: "test:simpleVM"
  - timeSeconds: 10
    event:
      type: DeploymentCommissioned
      consumerId: consumer1
      deploymentId: deployment1
      deployment_type: "test:simpleVM"
  - timeSeconds: 30
    event:
      type: DeploymentUpdated
      consumerId: consumer1
      deploymentId: deployment1
      deployment_type: "test:simpleVM"
  - timeSeconds: 50
    event:
      type: DeploymentCancellationRequested
      consumerId: consumer1
      deploymentId: deployment1
      deployment_type: "test:simpleVM"
...
```
The script to run can be configured.

The OGate Simulator can be used to implement the Chain Reactor without the need to host a test blockchain.

This Chain Reactor Reference implementation has a QA Mode, that will run a Simulation and Stop. With QA Mode this setup allows for building integration tests and running them in CD/CI.

Chain Reactors can build complete Tests Suites to cover all functionality of their chain reactors this way. 

### Reference implementation of Continuous Delivery of Chain Reactors

This Chain Reactor includes the [GitLab Pipeline](../../.gitlab-ci.yml) Stage `reactor_test` that will run an integration test suite for ACS RI Reactor.

These tests cover and end to end system with the following component flow:

```
DINFRA (simulated) -> OGate -> Chain Reactor -> Ansible -> ACS (Simulated)
```

Both Substrate and ACS are simulated.

#### Infrastructure without Simulation

When implementing a Chain Reactor for a platform that does not provide a Simulator there are 2 options often used:

- Nested virtualization: With this technique a Gitlab Runner (or similar) is configured directly on Metal with Libvirtd/KVM (or similar) with nested virtualization enabled. A minimal cluster of your infrastructure platform can be deployed over VMs and use those to run your tests against.
- Real Infrastructure: Your tests can run against real infrastructure. A small scale platform delivered for that very purpose, or when the infrastructure allows for properly isolated multitenancy, an account set for the purpose.  

## Future of ACS Chain Reactor

This Reference Implementation is being developed together with DINFRA, at this stage is very closely coupled with DINFRA and OGate.

For that reason all DINFRA, including OGate, generated API Clients and the ACS Chain reactor are packages of the same project.

Once greater maturity is achieved, the Chain Reactor will be an independent project that will just use Released DINFRA APIs. 