# W.I.P. DINFRA Keytool

## Introduction

During the course of Cloud operation it is common to exchange security information between people and systems.
This is a sensitive and complex process with important security implications.

Substrate keyring uses internally the Public Key algorithm `ed25519` which is widely used in the infrastructure world.
The aim of this package is to provide key interoperability between Substrate Keyrings and common infrastructure software.

It is the vision that Consumers may use the DINFRA blockchain to nominate Operators of infrastructure, and that infrastructure 
credentials can be automatically derived and maintained from this on-chain entries and accounts.

## Initial viability study

We are initially attempting interoperability with SSH, which can use also `ed25519`. We believe it is viable however
we are not succeeding with key portability, as reported in the following [Bug Report](https://github.com/polkadot-js/common/issues/1858)

The viability study is maintained as a set of tests.

## Interesting Infrastructure Software using ed25519

We have identified the following software as target for key interoperability:

- Secure Shell SSH, 
- JSON Web Tokens JWT, for API Authentication 
- IPSEC, for VPN and Secure connectivity
- WireGuard, for VPN connectivity

## Work in progress

Keytool is a medium priority initiative within DINFRA, and requires work with other community members.

