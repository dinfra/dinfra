// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Keyring } from '@polkadot/keyring';
import { waitReady } from '@polkadot/wasm-crypto';
import { mnemonicGenerate } from '@polkadot/util-crypto';

const crypto = require('crypto');
const fs = require('fs');
function test_log(msg) {
    // eslint-disable-next-line no-console
    console.log(msg);
}

/**
 * Test for key interoperability with various software pieces
 */
describe('Substrate Key tests', ()=>{
    let keyring: Keyring;

    beforeAll(async ()=>{
        await waitReady();
        keyring = new Keyring({ type: 'ed25519', ss58Format: 1 });
    });

    it('Generate a Substrate key pair test interoperability with NodeJS / OpenSSL', ()=>{
        // valid, is not supported since it is more-easily crackable)
        const mnemonic = mnemonicGenerate();
        const pair = keyring.addFromUri(mnemonic, { name: 'first pair' }, 'ed25519');

        test_log(pair.publicKey);

        // PCKCS8 is a well defined standard, the other encoding option, JSON seems propietary but provides similar content.
        const k8 = pair.encodePkcs8();
        // Write out to perform tests with OpenSSL
        // Command: openssl pkey -inform DER -outform PEM -in key.pkcs8 -out key.pem
        fs.writeFileSync('key.pkcs8', k8);

        const node_key = crypto.createPrivateKey({
            key: Buffer.from(k8),
            format: 'der',
            type: 'pkcs8',
        });

        expect(node_key).toBeTruthy();
    });
});
