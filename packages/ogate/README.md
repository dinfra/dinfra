# DINFRA OGate

The DINFRA OpenAPI Gateway. OGate is a DINFRA Collator sidecar that serves as the Interface between the DINFRA Parachain and the Chain Reactors. OGate Translates OpenAPI REST Requests from the Chain Reactors to DINFRA RPC Calls and Queries. 

## Introduction

The purpose of OGate is to reduce the complexity of the interface between the Chain Rectors and the DINFRA Blockchain.

OGate follows a sidecar pattern, implements and OpenAPI server, and translates the Requests into Substrate RPC Calls. OGate sidecar pattern is specialized for DINFRA, unlike the [generic sidecar](https://docs.substrate.io/reference/how-to-guides/tools/use-sidecar/) delivered with substrate. 

## Open API Client

OGate generates an OpenAPI Specification that can be used to generate DINFRA API Clients in any language, facilitating the implementation of Chain Reactors.

## Authentication

Chain Reactors authenticated with OGate using by using JWT to sign the API Calls. Current implementation uses a shared secret but JWT allows for publich key as well.

Note that amount its responsibilities, OGate will sign DINFRA parachain transactions therefore security is required between OGate and the Chain Reactors. 

### Authentication during Development and QA

If no SHARED Secret is configured via Environment Variables, a Well Known development token will be set by default.

3 development tokens for 3 different accounts (for chain reactor instances) will be generated wihtout expiry date. They can be used to test the API in the interactive playground.

```
[Nest] 1212513  - 07/26/2023, 4:59:13 PM    WARN [AuthGuard] Development secret token detect, DO NOT USE IN PRODUCTION
[Nest] 1212513  - 07/26/2023, 4:59:13 PM   DEBUG [AuthGuard] Dev token for dev-account-1 is eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiJkZXYtYWNjb3VudC0xIiwiaWF0IjoxNjkwMzgzNTUzfQ.C7UBxmvCZj0TXGLLyFd_pv42nmhKcjj8W4jl_ZpDCb4 never expires
[Nest] 1212513  - 07/26/2023, 4:59:13 PM   DEBUG [AuthGuard] Dev token for dev-account-2 is eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiJkZXYtYWNjb3VudC0yIiwiaWF0IjoxNjkwMzgzNTUzfQ.DFP_CbYLMBi-Gl_WCeMoUNWkasH8kV7NGM6-Xh-zYqs never expires
[Nest] 1212513  - 07/26/2023, 4:59:13 PM   DEBUG [AuthGuard] Dev token for dev-account-3 is eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiJkZXYtYWNjb3VudC0zIiwiaWF0IjoxNjkwMzgzNTUzfQ.C2EXd4a-flJgSTefj8AggtLCcT-jUq_FORdRVWiWAjE never expires
```

## DINFRA Simulator

OGate includes a SIMULATOR that facilitates development wihtout reguiring to host a whole substrate parachain and relay chan test environment.

The Simulator is very simple: includes a list of events that will be produced per Reactor account. 

When a reactor connects to OGate the simulator starts generating the scripted events.
