// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Test, TestingModule } from '@nestjs/testing';

import { OGateModule } from '../src/ogate.module';
import jestOpenAPI from 'jest-openapi';
import { configure } from '../src/ogate.config';

import * as fs from 'fs';

jest.setTimeout(120 * 1000);

describe('OGate end-to-end testing', () => {
    let app;

    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [OGateModule],
        }).compile();

        app = moduleFixture.createNestApplication();

        // Configure the app as in production and setup OpenAPI testing
        jestOpenAPI(configure(app, false));

        await app.init();
    });

    // For convenience, we generate the openapi specification document
    // only after having verified that some e2e tests are successful.

    it('Will create openapi specification', (done) => {
        const doc = configure(app, true);
        const outPath = 'dinfra-api-spec.json';
        fs.writeFile(outPath, JSON.stringify(doc), (error) => {
            if (error) throw error;
            else done();
        });
    });
});