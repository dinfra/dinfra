// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { OGateModule } from '../src/ogate.module';

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [OGateModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/version (GET)', () => {
        return request(app.getHttpServer())
            .get('/version')
            .expect(200);
    });
});
