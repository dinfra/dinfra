// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Injectable, Logger, OnModuleDestroy } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import * as yaml from 'js-yaml';
import { AxiosRequestConfig } from 'axios';


// TODO: this interface should also be cable of doing IPFS API, such as pinning, etc.
/**
 * Simple interface to an IPFS endpoint with content awareness
 * HTTP Content Gateway
 */
@Injectable()
export class IpfsService implements OnModuleDestroy {

    private MAX_IPFS_BYTES;
    private IPFS_GTW;
    private client;
    private readonly logger = new Logger(IpfsService.name);

    constructor(private readonly config: ConfigService, private readonly httpService: HttpService) {
        this.logger.log('Starting IPFS service');
        this.IPFS_GTW = config.get('OGATE_IPFS_GATEWAY');
        const sizeKb = config.get('OGATE_CID_MAX_SIZE_KB');
        this.MAX_IPFS_BYTES = sizeKb * 1024;
        this.logger.log(`IFPS Gateway at ${this.IPFS_GTW}`);
        this.logger.log(`CID Max Size ${sizeKb} KBytes`);
    }


    /**
     * Reads a CID Object expected in JSON or YAML.
     * This IPFS content request will assume a size-limited JSON or YAML object is to be retrieved.
     * When using YAML the content marker "---" is recommended for this method to return the right parsing error
     * Otherwise the method will attempt parsing both content types.
     * @param cid
     */
    async getObjectCid(cid: string): Promise<any> {

        const config: AxiosRequestConfig<any> = {
            responseType: 'text',
            baseURL: `${this.IPFS_GTW}/ipfs`,
            maxContentLength: this.MAX_IPFS_BYTES,
            maxBodyLength: this.MAX_IPFS_BYTES,
        };

        return new Promise((resolve, reject) => {
            this.httpService.get(cid, config).subscribe({
                next: response => {
                    let data;
                    let error;
                    const yaml_detected = response.data.startsWith('---');
                    // data = yaml.load(response.data);
                    if(!yaml_detected) {
                        try {
                            data = JSON.parse(response.data);
                            resolve(data);
                        } catch (e) {
                            error = e;
                        }
                    }
                    if(yaml_detected || error) {
                        try {
                            data = yaml.load(response.data);
                            resolve(data);
                        } catch (e) {
                            // will prioritize the JSON error unless YAML document was detected
                            error = yaml_detected ? e : error ? error : e;
                            reject(new Error(`Parsing Error: ${error.message}`));
                        }
                    }
                },
                error: reject,
            });
        });
    }


    onModuleDestroy(): any {
        // close client
    }

}
