// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { QueueService, NOTHING } from './queue.service';
import * as Joi from 'joi';

describe('Queue Service', () => {
    let queue: QueueService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_POLLING_INTERVAL: Joi.number().default(3),
                }),
            }),
            ],
            controllers: [],
            providers: [QueueService],
        }).compile();

        queue = app.get<QueueService>(QueueService);
    });

    describe('Queue Unit Tests', () => {

        it('Will get nothing if I just read an empty queue', async ()=>{
            const v = await queue.getFirst(0);
            expect(v).toEqual(NOTHING);
        });

        it('Can add and retrieve from the queue', async () => {
            queue.addLast(1, 10);
            queue.addLast(1, 11);
            let v = await queue.getFirst(1);
            expect(v).toEqual(10);
            v = await queue.getFirst(1);
            expect(v).toEqual(11);
        });

        it('Cannot read twice from the queue', async ()=>{
            // TODO: Could not implement this with expect().toThrow
            try {
                // Won't wait for the promise
                queue.getFirst(2);
                // Start reading another without waiting
                await queue.getFirst(2);
            } catch(e) {
                expect(e).toBeInstanceOf(Error);
            }
        });

        it('Will test that customers are independent', async ()=> {
            const results = await Promise.all([
                queue.getFirst(3),
                queue.getFirst(4),
                queue.getFirst(5),
                queue.getFirst(6),
                queue.getFirst(7),
            ]);

            results.forEach(v => {
                expect(v).toEqual(NOTHING);
            });
        });


    });
});
