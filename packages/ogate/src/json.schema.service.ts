// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Ajv from 'ajv';
import addFormats from 'ajv-formats';
import addErrors from 'ajv-errors';
import addKeywords from 'ajv-keywords';
import { SubscriptionStates, Events } from './ogate.types';
import { IpfsService } from './ipfs.service';

/**
 * The schema service makes it easy to validate JSON documents.
 * Currently the Simulator script is validated, but also the Deployment Descriptors will be validated as their type
 * will be the JSON Schema URl.
 * This service provides readable parsing errors.
 * It also provides custom format extensions that can be derived fromn type definitions.
 */
@Injectable()
export class JsonSchemaService {
    private ajv;

    private validators = {};

    constructor(private readonly config: ConfigService, private readonly ipfs: IpfsService) {
        this.ajv = this.buildAJV();
    }

    /**
     * Builds an Schema Compiler with DINFRA Extensions for format validation
     * @private
     */
    private buildAJV() {
        const ajv = new Ajv({ allErrors: true, verbose: true });
        addFormats(ajv);
        addErrors(ajv);
        addKeywords(ajv);

        // add a custom Format for DINFRA Events
        ajv.addFormat('dinfra-event', {
            type: 'string',
            validate: (x) => (Object.values(Events).includes(x as Events)),
        });
        // add a custom Format for DINFRA deployment states
        ajv.addFormat('dinfra-subscription-state', {
            type: 'string',
            validate: (x) => (Object.values(SubscriptionStates).includes(x as SubscriptionStates)),
        });

        return ajv;
    }

    /**
     * Builds a validator function from a schema file
     * Supports YAML Schemas, which is the preferred format due to is better readibility.
     * @param schemaFilePath
     * @private
     */
    private async buildSchemaValidator(schemaRef: SchemaRef) {
        let simulatorSchema;
        // for now we asume the schema is local
        // TODO: IPFS hosted schemas
        // TODO: Security implications
        if(schemaRef.source == 'file') {
            const schemaData: string = fs.readFileSync(schemaRef.path, 'utf8');
            simulatorSchema = yaml.load(schemaData);
        } else {
            simulatorSchema = await this.ipfs.getObjectCid(schemaRef.path);
        }
        return this.ajv.compile(simulatorSchema);
    }

    /**
     * Caches the compiled schemas. Schema validation may take place on each API request.
     * @param schemaFilePath
     * @private
     */
    private getCachedSchemaValidator(schemaRef:SchemaRef) {
        const cacheKey = schemaRef.source + '/' + schemaRef.path;
        if (this.validators[cacheKey]) {return this.validators[cacheKey];} else {
            const validator = this.buildSchemaValidator(schemaRef);
            this.validators[cacheKey] = validator;
            return validator;
        }
    }

    /**
     * Validates an object with a given schema
     * caches validators
     * @param schemaFilePath
     * @param object
     */
    public async validate(schemaRef: SchemaRef, object):Promise<ValidatorResult> {
        const validator = await this.getCachedSchemaValidator(schemaRef);
        const valid = validator(object);
        let message = 'Validation OK';
        if(!valid) message = `JSON Validation Error: ${validator.errors.map(e=>(`${e.instancePath}: ${e.message}`)).join(', ')}`;
        return {
            valid,
            message,
        };
    }

    /**
     * Throws an Error if validation fails, convenience method.
     * @param schemaFilePath
     * @param object
     */
    public async ensureValid(schemaRef: SchemaRef, object) {
        const result = await this.validate(schemaRef, object);
        if(!result.valid) throw new Error(result.message);
        return result;
    }

}

export class ValidatorResult {
    valid: boolean;
    message: string;
}

export type SchemaRef = {
    source: 'file' | 'ipfs';
    path: string
}
