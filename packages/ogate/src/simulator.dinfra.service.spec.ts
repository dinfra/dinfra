// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { QueueService } from './queue.service';
import * as Joi from 'joi';
import { DinfraSimulatorService, DinfraSimulation } from './simulator.dinfra.service';
import { JsonSchemaService } from './json.schema.service';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';

describe('Simulator Service', () => {
    let dinfraSimulator: DinfraSimulatorService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [HttpModule, ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_SIMULATOR_SCRIPT: Joi.string().default('test/scripts/simulator.yml'),
                }),
            }),
            ],
            controllers: [],
            providers: [QueueService, DinfraSimulatorService, JsonSchemaService, IpfsService],
        }).compile();

        dinfraSimulator = app.get<DinfraSimulatorService>(DinfraSimulatorService);
    });

    it('Will test the Simulation Script', ()=>{

        const sim = new DinfraSimulation(dinfraSimulator.getSimulationScript(), 'ALICE');

        const step_times = sim.getStepTimes();

        const no_step_time = Math.floor((step_times[0] + step_times[1]) / 2);

        let event;

        event = sim.getEvent(step_times[0]);
        expect(event.type).toBeTruthy();

        event = sim.getEvent(no_step_time);
        expect(event).toBeUndefined();

        event = sim.getEvent(step_times[1]);
        expect(event.type).toBeTruthy();

    });

    it('Will test the deployment state in Simulation Scripts', ()=>{

        const sim = new DinfraSimulation(dinfraSimulator.getSimulationScript(), 'BOB');

        sim.getSubscriptionList();

    });


});