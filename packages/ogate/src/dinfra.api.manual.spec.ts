// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { DinfraAPIService } from './dinfra.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { ApiPromise } from '@polkadot/api';
import { Keyring } from '@polkadot/api';
import { blake2AsHex } from '@polkadot/util-crypto';


/**
 * Development tests with API Primitives
 * We test the api primitives that we will use across DINFRA client implementation
 */
describe('DinfraService', () => {
    let api: ApiPromise;
    let alice, bob;

    // Setup some Development Accounts
    const keyring = new Keyring({ type: 'sr25519' });

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_DINFRA_RPC_URL: Joi.string().default('ws://localhost:37741'),
                }),
            }),
            ],
            providers: [ DinfraAPIService ],
        }).compile();

        api = module.get<ApiPromise>('DINFRA_API');
        await api.isReady;
        // create tests keys
        alice = keyring.addFromUri('//Alice', { name: 'Alice development account' });
        bob = keyring.addFromUri('//Bob', { name: 'Bob development account' });
    });

    it('api should be defined and ready', async () => {
        expect(api).toBeDefined();
    });

    // Here we are testing the potential for storage of deployment types as preimages
    // However, it is not clear what the benefit is as this information is not used for
    // Consensus, ipfs seems more appropriate

    xit('adds a deployment type as preimage', async () => {
        // Take a sample preimage
        const test = 'Hello World';
        // Workout the Hash of our Preimage
        const testHash = blake2AsHex(test);

        // We note the preimage
        const txHash = await api.tx.preimage.notePreimage(test).signAndSend(alice, { nonce: -1 });
        expect(txHash).toBeTruthy();

        // Ensure we can retrive the status of our preimage by hash
        const status = await api.query.preimage.statusFor(testHash);
        expect(status.isEmpty).toBeFalsy();
        const statush = status.toHuman() as any;
        const testLength = parseInt(statush.Unrequested.len);
        expect(testLength).toEqual(test.length);

        // Ensure we can get the preimage by Hash and 11 of length.
        const pi = await api.query.preimage.preimageFor([testHash, test.length]);
        expect(pi.isEmpty).toBeFalsy();
        const preimage = pi.toHuman();
        expect(test).toEqual(preimage);

    });

    it('tests creating a transaction from a development account', async () => {

        // Select a random balance
        const amount = Math.floor(Math.random() * 10e6);

        // Alice to Bob
        let txHash = await api.tx.balances
            .transfer(bob.publicKey, amount)
            .signAndSend(alice, { nonce: -1 });
        expect(txHash).toBeTruthy();

        // Bob to Alice
        txHash = await api.tx.balances
            .transfer(alice.publicKey, amount)
            .signAndSend(bob, { nonce: -1 });
        expect(txHash).toBeTruthy();

    });

    afterAll(async () => {
        await api.disconnect();
    });

});
