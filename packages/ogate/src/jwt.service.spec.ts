// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import { DEVELOPMENT_SECRET } from './ogate.module';
import { JwtModule, JwtService } from '@nestjs/jwt';

/**
 * The JWT Service is actually an internal NestJS component.
 * However it is very important that we understand its behaviour, and that if it changes in the future we can detect it.
 * Thats is wny we have this tests even thou we have not developed that service.
 */
describe('JWT Service', () => {
    let jwt: JwtService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_SECRET_TOKEN: Joi.string().default(DEVELOPMENT_SECRET),
                }),
            }),
            JwtModule.registerAsync({
                imports: [ConfigModule],
                useFactory: async (config: ConfigService) => ({
                    global: true,
                    secret: config.get<string>('OGATE_SECRET_TOKEN'),
                }),
                inject: [ConfigService],
            }),
            ],
            controllers: [],
            providers: [],
        }).compile();

        jwt = app.get<JwtService>(JwtService);
    });

    it('Will check the module was created', ()=>{
        expect(jwt).toBeTruthy();
    });

    it('Will encode, decode, and verify a token', ()=>{
        const payload = {
            accountId: 1,
        };
        const token = jwt.sign(payload, { secret: DEVELOPMENT_SECRET });
        jwt.verify(token, { secret:DEVELOPMENT_SECRET });
        const info:any = jwt.decode(token);
        expect(info.accountId).toEqual(payload.accountId);
    });

    it('Will encode, cant decode with wrong key', ()=>{
        const payload = {
            accountId: 1,
        };
        const token = jwt.sign(payload, { secret: DEVELOPMENT_SECRET });
        expect(()=>{
            jwt.verify(token, { secret:'Wrong secret' });
        }).toThrow('invalid signature');
    });

    it('Will encode, cant decode expired token', (done)=>{
        const payload = {
            accountId: 1,
        };
        const token = jwt.sign(payload, { secret: DEVELOPMENT_SECRET, expiresIn: '2s' });
        // The Following verification will work, because the token is valid
        jwt.verify(token, { secret:DEVELOPMENT_SECRET });
        setTimeout(()=>{
            expect(()=>{
                // this verification won't work because the token has expired
                jwt.verify(token, { secret:DEVELOPMENT_SECRET });
            }).toThrow('expired');
            done();
        }, 3000);
    });


});
