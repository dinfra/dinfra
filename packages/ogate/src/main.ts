// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { NestFactory } from '@nestjs/core';
import { OGateModule } from './ogate.module';
import { ConfigService } from '@nestjs/config';

import { configure } from './ogate.config';

async function bootstrap() {
    const app = await NestFactory.create(OGateModule);
    configure(app);
    await app.listen(app.get(ConfigService).get('OGATE_PORT'));
}
bootstrap();
