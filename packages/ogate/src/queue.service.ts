// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Injectable, OnModuleDestroy } from '@nestjs/common';
import Deferred from 'my-deferred';
import { ConfigService } from '@nestjs/config';

/**
 * The Queue service is used to implement a busy wait push system.
 * The goal is that DINFRA clients can receive PUSH notifications with an OpenAPI client only
 * So, the implementation of the push system must be based on plain HTTP.
 *
 * Events are delivered as they arrive, without delay. The implementation uses Deferred promises
 * to deliver events as they arrive. Basically, the response promise to a request is stored and resolved
 * when an event arrives, something that happens at a different control flow, when events are notified from Dinfra or
 * the simulator.
 *
 * Promises are resolved in a known maximum period of time, so that underlying HTTP protocol can continue to work as usual.
 * This is achieved by a timer that will inject a NoEvent event if there is nothing in the Queue. Reactors will just request
 * next events in a loop ignoring the NonEvent event, which in this context is called NOTHING.
 *
 */
@Injectable()
export class QueueService implements OnModuleDestroy {
    private clients = {};
    private readonly interval;

    constructor(private readonly config: ConfigService) {
        this.interval = config.get('OGATE_POLLING_INTERVAL');
    }

    /**
     * adds an event to the queue
     * @param client the requesting reactor
     * @param object the event object
     */
    public addLast(client, object) {
        this.checkClient(client);
        this.clients[client].addLast(object);
    }

    /**
     * retrieves the first event in the queue
     * @param client the requesting reactor
     */
    public async getFirst(client) {
        this.checkClient(client);
        return this.clients[client].getFirst();
    }

    /**
     * checks if a queue is already created for a given client
     * @param client the requesting reactor
     * @private
     */
    private checkClient(client) {
        if (!this.clients[client]) {
            this.clients[client] = new Queue(this.interval);
        }
    }

    /**
     * On destruction, all data structures linked to client must also be destroyed.
     */
    onModuleDestroy(): any {
        const clientIds = Object.keys(this.clients);
        clientIds.forEach(c => {
            this.clients[c].destroy();
        });
    }

}

/**
 * Instance of a Queue.
 * This is simply a Javascript Array, but it also holds the interval that generates the NoEvent
 */
class Queue {
    private queue = [];
    private readonly interval;
    private readonly intervalId;
    private readonly nothing;
    private deferred:Deferred<any>;

    constructor(interval) {
        this.interval = interval;

        this.intervalId = setInterval(()=>{
            // If the queue is empty push nothing.
            if(!this.queue.length) this.addLast(NOTHING);
        }, this.interval * 1000);
    }

    /**
     * Adds an object event to the queue.
     * @param object
     */
    public addLast(object) {
        if(this.deferred) {
            this.deferred.resolve(object);
            // done, forget about the request.
            this.deferred = undefined;
        } else {
            this.queue.push(object);
        }
    }

    /**
     * Retrieves what is in the queue of waits until something is queued.
     * This will return before Interval
     */
    public async getFirst() {
        if (this.queue.length) {
            // There is data waiting to be served,
            return Promise.resolve(this.queue.shift());
        } else {
            // Is There is a promise pending?
            // supporting this would mean distributed queue pattern
            // TODO: Authentication can ensure only one session per
            if(this.deferred) {
                // When we get overlapping requests, we reject the pending and throw on the caller
                this.deferred.resolve(NOTHING);
                this.deferred = null;
                throw new RequestCancelledError();
            }
            // Create a Pending Promise for later
            this.deferred = new Deferred<any>();
            return this.deferred.promise;
        }
    }

    public destroy() {
        clearInterval(this.intervalId);
    }
}


/**
 * The type to return when there is NOTHING
 */
export const NOTHING = null;

export class RequestCancelledError extends Error {
    constructor() {
        super('The pending request was cancelled by a new request');
    }
}