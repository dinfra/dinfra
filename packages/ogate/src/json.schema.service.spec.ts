// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import * as fs from 'fs';
import * as yaml from 'js-yaml';

import { Test, TestingModule } from '@nestjs/testing';
import { JsonSchemaService } from './json.schema.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import { DINFRA_SIMULATOR_SCRIPT_SCHEMA } from './simulator.dinfra.service';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';

function test_log(msg) {
    // eslint-disable-next-line no-console
    console.log(msg);
}


/**
 * JSON Schema validation is used to validate the simulator script, but will also be used as main
 * deployment descriptor type.
 */
describe('Json Schema Service and Utilities', () => {
    let schemaService: JsonSchemaService;
    let config: ConfigService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [HttpModule, ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_SIMULATOR_SCRIPT: Joi.string().default('test/scripts/simulator.yml'),
                }),
            }),
            ],
            controllers: [],
            providers: [JsonSchemaService, IpfsService],
        }).compile();

        schemaService = app.get<JsonSchemaService>(JsonSchemaService);
        config = app.get<ConfigService>(ConfigService);

    });

    it('Will check a simple validation', async ()=>{
        const simulatorScript = {
            simulatorScript: [
                {
                    providerAccountId: 'provider1',
                    initialState: [
                        {
                            subscriberAccountId: 'deployment1a',
                            deploymentTypeId: 'type1',
                            subscriptionState: 'Created',
                            deploymentDescriptor: {},
                        },
                    ],
                    simulationSteps:[
                        {
                            timeSeconds: 1,
                            subscriptionEvent: {
                                eventType: 'SubscriptionCreated',
                                subscriberAccountId: 'consumer1',
                            },
                        },
                    ],
                },
            ],
        };

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);

        test_log(result.message);
        expect(result.valid).toBeTruthy();
    });

    it('Will check a validation error, invalid dinfra event', async ()=>{
        const simulatorScript = {
            simulatorScript: [
                {
                    providerAccountId: 'provider1',
                    initialState: [
                        {
                            subscriberAccountId: 'deployment1a',
                            deploymentTypeId: 'type1',
                            subscriptionState: 'Created',
                            deploymentDescriptor: {},
                        },
                    ],
                    simulationSteps:[
                        {
                            timeSeconds: 1,
                            subscriptionEvent: {
                                eventType: 'SubscriptionCreatedXXX',
                                subscriberAccountId: 'consumer1',
                            },
                        },
                    ],
                },
            ],
        };

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);

        test_log(result.message);
        expect(result.message).toContain('format');
        expect(result.message).toContain('dinfra');
        expect(result.valid).toBeFalsy();
    });

    it('Will check a validation error, missing property', async ()=>{
        const simulatorScript = {
            simulatorScript: [
                {
                    providerAccountId: 'provider1',
                    initialState: [
                        {
                            subscriberAccountId: 'deployment1a',
                            deploymentTypeId: 'type1',
                            subscriptionState: 'Commissioned',
                            deploymentDescriptor: {},
                        },
                    ],
                    simulationSteps:[
                        {
                            timeSeconds: 1,
                            subscriptionEvent: {
                                subscriberAccountId: 'consumer1',
                            },
                        },
                    ],
                },
            ],
        };

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);

        test_log(result.message);
        expect(result.message).toContain('required property');
        expect(result.message).toContain('eventType');
        expect(result.valid).toBeFalsy();
    });

    it('Will check a validation error, forbidden extra property', async ()=>{
        const simulatorScript = {
            simulatorScript: [
                {
                    providerAccountId: 'provider1',
                    initialState: [
                        {
                            subscriberAccountId: 'deployment1a',
                            deploymentTypeId: 'type1',
                            subscriptionState: 'Created',
                            deploymentDescriptor: {},
                        },
                    ],
                    simulationSteps:[
                        {
                            timeSeconds: 1,
                            subscriptionEvent: {
                                eventType: 'SubscriptionCreated',
                                subscriberAccountId: 'consumer1',
                                invalidExtraProperty: 'INVALID',
                            },
                        },
                    ],
                },
            ],
        };

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);

        test_log(result.message);
        expect(result.message).toContain('additional properties');
        expect(result.valid).toBeFalsy();
    });


    it('Will check validation error, empty object', async ()=>{
        const simulatorScript = {};

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);
        test_log(result.message);
        expect(result.message).toContain('required property');
        expect(result.message).toContain('simulatorScript');
        expect(result.valid).toBeFalsy();
    });


    it('Will Validate the Simulation Test Script', async ()=>{
        const simulatorScriptData: string = fs.readFileSync(config.get<string>('OGATE_SIMULATOR_SCRIPT'), 'utf8');
        const simulatorScript = yaml.load(simulatorScriptData);

        const result = await schemaService.validate(DINFRA_SIMULATOR_SCRIPT_SCHEMA, simulatorScript);
        test_log(result.message);
        expect(result.valid).toBeTruthy();
    });


});