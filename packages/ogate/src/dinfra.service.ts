// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Inject, Injectable, Logger, OnModuleDestroy } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiPromise, WsProvider } from '@polkadot/api';
import { DinfraService, Event, Events, SubscriptionDetail, SubscriptionState, SubscriptionStates } from './ogate.types';
import { NOTHING, QueueService } from './queue.service';
import { JsonSchemaService } from './json.schema.service';
import { DEVELOPMENT_SECRET } from './ogate.module';
import { Keyring } from '@polkadot/keyring';
import { IpfsService } from './ipfs.service';
import { SubscriptionDescriptorMissing, SubscriptionDescriptorNotCompliant } from './ogate.controller';


/**
 * Connects OGate to DINFRA Parachain,
 * DINFRA is currently being simulated with the simulator.
 */
@Injectable()
export class DinfraParachainService implements OnModuleDestroy, DinfraService {

    // TODO: this subscription trackings would require persistence. redis?
    // We keep track of the providers we are serving
    private providers = {};
    // we keep track of the subscribers we are serving
    private subscribers = {};
    // use developer friendly hooks
    private development;
    private DEV_ADDRESSES;
    private keyring = new Keyring({ type: 'sr25519', ss58Format: 42 });
    // unsubscribe callbacks
    private blockUnsub;
    private eventUnsub;
    // debug aids
    private readonly logger = new Logger(DinfraParachainService.name);
    private readonly tracing = true;


    constructor(
        @Inject('DINFRA_API') private api,
        private readonly config: ConfigService,
        private readonly queue: QueueService,
        private readonly schemaService: JsonSchemaService,
        private readonly ipfs: IpfsService) {
        // validated the script
        this.logger.log('DINFRA Parachain Service Started');
        this.development = (config.get<string>('OGATE_SECRET_TOKEN') == DEVELOPMENT_SECRET);
        this.doListenToBlocks();
        this.doListenToEvents();

        // Initialize development
        if (this.development) {
            this.DEV_ADDRESSES = DEV_ACCOUNT_NAMES.map(a => this.keyring.addFromUri(nameToUri(a)).address);
        }
    }

    private async doListenToBlocks() {
        this.blockUnsub = await this.api.rpc.chain.subscribeNewHeads((header) => {
            if (this.tracing) this.logger.debug(`DINFRA Block: #${header.number}`);
        });
    }

    private async doListenToEvents() {
        // TODO: subscribing to every event wont be feasible in the long term,
        // Subscribing storage udpates may be a better strategy
        this.eventUnsub = this.api.query.system.events(async (events) => {

            // Iterate through the events
            for (const record of events) {
                const { event } = record;

                // Filter DINFRA Events only
                if (['dinfraSubscription', 'dinfraProvider'].includes(event.section)) {
                    let ep = await this.parseEventForProvider(event);
                    if(ep) {
                        ep = this.formatEvent(ep);
                        if (this.filterEvent(ep)) {
                            ep.provider ?
                                this.logger.log(`Received ${ep.subject} for ${ep.provider}`) :
                                this.logger.log(`Received ${ep.subject}`);
                            this.processEvent(ep);
                        } else {
                            this.logger.log(`${ep.subject} event filtered out.`);
                        }
                        if (this.tracing) this.logger.debug(ep);
                    }

                }
            }
        });
    }

    // Parse Event
    // TODO: this work should rely on automatic type generation with typegen
    // the package is currently unstable
    // TODO: type equivalence/common interface between DINFRA and OGRATE interfaces
    async parseEventForProvider(event): Promise<EventForProvider> {
        const eventName = `${event.section}.${event.method}`;
        const eventBody = event.toHuman();

        const eventData = eventBody.data;
        if (this.tracing) {
            this.logger.debug(`Received substrate event ${eventName}`);
            this.logger.debug(eventData);
        }

        // or subscriptions we are serving

        switch (eventName) {
        case 'dinfraSubscription.SubscriptionCreated':
            return {
                subject: eventName,
                provider: undefined,
                event: {
                    type: Events.SubscriptionCreated,
                    timestamp: this.now(),
                    deploymentId: `${eventBody.data[1]}`,
                    subscriberAccountId: eventData[0],
                },
            };
        case 'dinfraSubscription.SubscriptionAwarded':
            return {
                subject: eventName,
                provider: eventData[1]?.Deployed?.provider,
                event: {
                    type: Events.SubscriptionAwarded,
                    timestamp: this.now(),
                    subscriberAccountId: eventData[0],
                },
            };
        case 'dinfraSubscription.SubscriptionUpdated':
            return {
                subject: eventName,
                provider: eventData[1]?.Deployed?.provider,
                event: {
                    type: Events.SubscriptionUpdated,
                    timestamp: this.now(),
                    subscriberAccountId: eventData[0],
                },
            };
        case 'dinfraSubscription.SubscriptionTerminated':
            return {
                subject: eventName,
                provider: eventData[1]?.Terminated?.provider,
                event: {
                    type: Events.SubscriptionTerminated,
                    timestamp: this.now(),
                    subscriberAccountId: eventData[0],
                },
            };
        case 'dinfraSubscription.SubscriptionDeleted':
            return {
                subject: eventName,
                event: {
                    type: Events.SubscriptionDeleted,
                    timestamp: this.now(),
                    subscriberAccountId: eventData[0],
                },
            };
        default:
            this.logger.log(`Event ${eventName} ignored`);
        }
    }


    /**
     * Process events after first filtering.
     * @param ep
     * @private
     */
    private processEvent(ep: EventForProvider) {
        // Submit events for our providers
        if(ep.provider) {
            // We add the subscription to the filtering list.
            if(ep.event.subscriberAccountId) this.checkSubscription(ep.event.subscriberAccountId);
            // we pass the event to the provider queue
            if(Object.keys(this.providers).includes(ep.provider)) this.queue.addLast(ep.provider, ep.event);
        }

        if(ep.event.type == Events.SubscriptionDeleted) {
            // stop listening to this subscription
            delete this.subscribers[ep.event.subscriberAccountId];
            this.logger.debug(`No longer listening to subscription event for ${this.formatAccountId(ep.event.subscriberAccountId)}`);
        }

    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async getSubscription(providerAccountId, subscriberAccountId): Promise<SubscriptionDetail> {
        if (this.tracing) this.logger.debug(`Requested Subscription detail of ${this.formatAccountId(subscriberAccountId)} by ${this.formatAccountId(providerAccountId)}`);

        if (this.development) {
            // during development for api calls we use addresses only
            subscriberAccountId = this.devNameToAcc(subscriberAccountId);
        }

        const subscriptionInfo = (await this.api.query.dinfraSubscription.subscriptions(subscriberAccountId)).toHuman();
        const subscriptionState = (await this.api.query.dinfraSubscription.subscriptionsState(subscriberAccountId)).toHuman();
        const deploymetType = (await this.api.query.dinfraProvider.deploymentTypes(subscriptionInfo.deploymentType)).toHuman();

        // Extract the state
        const state = typeof subscriptionState == 'string' ? subscriptionState as SubscriptionStates : Object.keys(subscriptionState)[0] as SubscriptionStates;

        const deploymentTypeCID = deploymetType.deploymentType;
        
        let deploymentDescriptor;
        
        try {
            deploymentDescriptor = await this.ipfs.getObjectCid(subscriptionInfo.deploymentDescriptor);
        } catch (e) {
            this.logger.warn(`Could not retrieve the deployment descriptor of subscription ${this.formatAccountId(subscriberAccountId)} `);
            await this.cancelSubscription(providerAccountId, subscriberAccountId);
            throw new SubscriptionDescriptorMissing(subscriberAccountId);
        }


        // Validate the deploymentDescriptor complies with the deployment type
        try {
            await this.schemaService.ensureValid({ source:'ipfs', path:deploymentTypeCID }, deploymentDescriptor);
        } catch (e) {
            this.logger.warn(`The subscription ${this.formatAccountId(subscriberAccountId)} does not comply with its deployment type`);
            await this.cancelSubscription(providerAccountId, subscriberAccountId);
            throw new SubscriptionDescriptorNotCompliant(subscriberAccountId);
        }

        const ret = {
            deploymentDescriptor: JSON.stringify(deploymentDescriptor),
            deploymentType: deploymetType.deploymentType,
            lastUpdate: this.now(),
            state: state,
            subscriberAccountId: subscriberAccountId,
        };

        if(this.tracing) {
            this.logger.debug('Returning Subscription:');
            this.logger.debug(ret);
        }
        return ret;

    }

    // TODO: the deployment lifecycle needs to be completed with more real-world scenarios
    /**
     * Cancels a subscription, normally while in award state due to non-compliance
     * @param providerAccountId
     * @param subscriberAccountId
     */
    async cancelSubscription(providerAccountId, subscriberAccountId) {
        // Cancel only once if multiple events in a row
        // unsubscribe by subscription (yet we may receive by provider)
        if(this.subscribers[this.accToDevName(subscriberAccountId)]) {
            delete this.subscribers[this.accToDevName(subscriberAccountId)];
            const provider = this.getProviderKey(providerAccountId);
            if (this.development) subscriberAccountId = this.devNameToAcc(subscriberAccountId);
            const nonce = await this.api.rpc.system.accountNextIndex(provider.address);
            const unsub = await this.api.tx.dinfraSubscription.terminateSubscription(subscriberAccountId).signAndSend(provider, { nonce }, (result) =>{
                if(result.status.isFinalized) {
                    this.logger.warn(`Subscription ${this.formatAccountId(subscriberAccountId)} cancelled, could not be deployed.`);
                    unsub();
                }
            });
        }
    }

    // a keyright for production is required
    // should have all reactors keys
    // or perhaps a master key with proxy settings
    /**
     * Returns the keypair for a provider
     * @param providerAccountId
     * @private
     */
    private getProviderKey(providerAccountId) {
        // @ts-ignore
        if(this.development) {return this.keyring.addFromUri(nameToUri(this.accToDevName(providerAccountId)));}
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async getSubscriptionList(providerAccountId): Promise<Array<SubscriptionState>> {
        // TODO: future functionality for "refreshing" all deployments
        // also to ensure all deployments are in desired state, perhaps after re-starting a reactor, etc.
        return undefined;
    }

    public async getEvent(providerAccountId):Promise<Event> {
        this.checkSession(providerAccountId);
        let e = await this.queue.getFirst(providerAccountId);
        // It converts the queue NOTING into an API Event.
        if(e == NOTHING) {
            e = {
                type: Events.NoEvent,
                timestamp: this.now(),
            };
        }
        return e;
    }

    public getVersion(): string {
        return this.config.get('npm_package_version');
    }

    /**
     * Turns on filtering on this account ID as it is working with us.
     * @param providerAccountId
     * @private
     */
    private checkSession(providerAccountId) {
        if (!(providerAccountId in this.providers)) {
            // Create a provider store
            this.providers[providerAccountId] = {};
            this.logger.log(`Listening to events for provider ${this.formatAccountId(providerAccountId)}`);
            return;
        }
    }

    /**
     * Turns on filtering on this account ID as it is working with us.
     * @param providerAccountId
     * @private
     */
    private checkSubscription(subscriberAccountId) {
        if (!(subscriberAccountId in this.subscribers)) {
            // Create a provider store
            this.subscribers[subscriberAccountId] = {};
            this.logger.log(`Listening to subscription events of ${this.formatAccountId(subscriberAccountId)}`);
            return;
        }
    }

    /**
     * Returns a Unix epoch timestamp for events.
     * @private
     */
    private now() {
        return Math.floor(Date.now() / 1000);
    }

    /**
     * Early filtering of events.
     * Basically they need to relate to either providers or subscribers tracked
     * It is OK to leave false positives on, they will be filtered later on.
     * @param eventName
     * @private
     */
    private filterEvent(ep: EventForProvider) {
        return Object.keys(this.providers).includes(ep.provider) || Object.keys(this.subscribers).includes(ep.event.subscriberAccountId);
    }

    /**
     * Check if we need to translate accounts during development sessions
     * @param eventBody
     * @private
     */
    private formatEvent(ep:EventForProvider): EventForProvider {
        // Switch to development account names in development
        if(this.development) {
            if (ep.provider) ep.provider = this.accToDevName(ep.provider);
            if (ep?.event?.subscriberAccountId) ep.event.subscriberAccountId = this.accToDevName(ep.event.subscriberAccountId);
        }
        return ep;
    }

    /**
     * Formats accounts IDs prior to logs and human intended outputs
     * @param accountId
     * @private
     */
    private formatAccountId(accountId) {
        // when in dev, use dev names.
        return this.accToDevName(accountId);
    }


    /**
     * Scan Addresses and replaces by Development friendly names before presentation
     * @param accountId
     * @private
     */
    public accToDevName(accountId) {
        // only when in development mode
        if (!this.development) return accountId;
        const i = this.DEV_ADDRESSES.indexOf(accountId);
        if(i >= 0) return DEV_ACCOUNT_NAMES[i];
        else return accountId;
    }
    
    public devNameToAcc(accountId) {
        // only when in development mode
        if (!this.development) return accountId;
        const i = DEV_ACCOUNT_NAMES.indexOf(accountId);
        if(i >= 0) return this.DEV_ADDRESSES[i];
        else return accountId;
    }

    async onModuleDestroy() {
        this.blockUnsub();
        this.eventUnsub();
        this.logger.log('Closing API Connection...');
        await this.api.disconnect();
    }


}

type EventForProvider = {
    subject: string,
    provider?: string,
    event: Event
}

/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 */
export const DinfraAPIService = {
    provide: 'DINFRA_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('OGATE_DINFRA_RPC_URL');
        const logger = new Logger('DINFRA_API');
        const wsProvider = new WsProvider(endpoint);
        const api = await ApiPromise.create({ provider: wsProvider });
        await api.isReady;
        logger.log(`DINFRA RPC API Ready with endpoint: ${endpoint}`);
        return api;
    },
    inject: [ConfigService],
};

const DEV_ACCOUNT_NAMES = [
    'ALICE', 'ALICE_STASH',
    'BOB', 'BOB_STASH',
    'CHARLIE', 'DAVE',
    'EVE', 'FERDIE',
];

function nameToUri(name) {
    name = name.toLowerCase();
    name = name.charAt(0).toUpperCase() + name.slice(1);
    return `//${name}`;
}
