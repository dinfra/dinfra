// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import * as Joi from 'joi';
import { OGateController } from './ogate.controller';
import { DinfraSimulatorService } from './simulator.dinfra.service';
import { DinfraAPIService, DinfraParachainService } from './dinfra.service';
import { QueueService } from './queue.service';
import { JsonSchemaService } from './json.schema.service';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';

export const DEVELOPMENT_SECRET = 'default development token, never use in production';

/**
 * The Core module includes a set of generic modules used across the OGate
 * These modules are either very generic or NestJS provided
 */
@Module({
    imports: [ConfigModule.forRoot({
        isGlobal: true,
        validationSchema: Joi.object({
            NODE_ENV: Joi.string()
                .valid('development', 'production', 'test')
                .default('development'),
            OGATE_PORT: Joi.number().default(7300),
            OGATE_GLOBAL_PREFIX: Joi.string().default('dinfra'),
            OGATE_SECRET_TOKEN: Joi.string().default(DEVELOPMENT_SECRET),
            OGATE_DINFRA_RPC_URL: Joi.string().default('ws://localhost:37741'),
            OGATE_POLLING_INTERVAL: Joi.number().default(15),
            OGATE_SIMULATOR_SCRIPT: Joi.string().default('test/scripts/simulator.yml'),
            OGATE_IPFS_GATEWAY: Joi.string().default('http://localhost:8080'),
            OGATE_CID_MAX_SIZE_KB: Joi.number().default(1024),
        }),
    }),
    JwtModule.registerAsync({
        imports: [ConfigModule],
        useFactory: async (config: ConfigService) => ({
            global: true,
            secret: config.get<string>('OGATE_SECRET_TOKEN'),
        }),
        inject: [ConfigService],
    }),
    ],
    exports: [ConfigModule, JwtModule],
})
export class CoreModule {}


/**
 * Shared Modules across all OGate configurations.
 */

@Module({
    imports: [CoreModule, HttpModule],
    providers: [QueueService, JsonSchemaService, IpfsService],
    exports: [QueueService, JsonSchemaService, IpfsService],
})
class OGateCommonModule {}

/**
 * Configurable OGate Module.
 * This module will be dynamically configured to serve the requested use case: Simulator, Parachain, etc.
 */
@Module({})
export class ConfigurableOGateModule {
    static register():DynamicModule {
        // Are we simulating dinfra?
        const isSimulator = (process.env.OGATE_MODE || 'PARACHAIN').toUpperCase() == 'SIMULATOR';

        const ogate_provider = {
            provide: 'DINFRA_SERVICE',
            useClass: isSimulator ? DinfraSimulatorService : DinfraParachainService,
        };

        const ogate_providers: Array<any> = [ogate_provider];

        // Add the API for parachain config
        if(!isSimulator) {
            ogate_providers.push(DinfraAPIService);
        }

        return {
            module: ConfigurableOGateModule,
            controllers: [OGateController],
            imports: [ OGateCommonModule, CoreModule ],
            providers: ogate_providers,
        };
    }
}

/**
 * The module allows to configure the OGate service with different components.
 * Here we decide if the server is starting in production mode, using the DINFRA service,
 * or in development/testing mode using the Simulator instead.
 *
 * The Configuration is Dynamic and has 2 main modes: Simulator and Parachain
 *
 */
@Module({
    imports: [ConfigurableOGateModule.register()],
})
export class OGateModule {}

