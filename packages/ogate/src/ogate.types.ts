// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { ApiProperty } from '@nestjs/swagger';


/**
 *
 * Interface for different implementations of DinfraService
 *
 */

export interface DinfraService {
    getVersion(): string;
    getEvent(providerAccountId):Promise<Event>;
    getSubscriptionList(providerAccountId): Promise<Array<SubscriptionState>>;
    getSubscription(providerAccountId, subscriberAccountId): Promise<SubscriptionDetail>;
}

/**
 * This module declares Dinfra types and their documentation.
 * Note that the documentation here goes to the API documentation.
 */

export class Version {
  @ApiProperty({
      description: 'The semantic version from ogate package.json',
      type: 'string',
  })
      version: string;
}


export enum Events {
    NoEvent= 'NoEvent',
    SubscriptionCreated='SubscriptionCreated',
    SubscriptionUpdated='SubscriptionUpdated',
    SubscriptionAwarded='SubscriptionAwarded',
    SubscriptionDeployed='SubscriptionDeployed',
    SubscriptionTerminated='SubscriptionTerminated',
    SubscriptionDeleted='SubscriptionDeleted',

    // Events used by the Simulator, used for CD/CI and QA
    SimulationStarted='SimulationStarted',
    SimulationFinished='SimulationFinished',
}

export class Event {
    @ApiProperty({
        description: 'Type of Event',
        enum: Events,
    })
        type: string;

    @ApiProperty({
        description: 'The deploymentId this event relates to',
        required: false,
    })
        deploymentId?: string;

    @ApiProperty({
        description: 'The subscriberAccountId this event relates to',
        required: false,
    })
        subscriberAccountId?: string;

    @ApiProperty({
        description: 'timestamp of the event',
    })
        timestamp: number;
}

/**
 * The possible states a deployment can be at
 */
export enum SubscriptionStates {
    Created='Created',
    Rejected='Rejected',
    Awarded='Awarded',
    Deployed='Deployed',
    Terminated='Terminated',
    Cancelled='Cancelled',
}

/**
 * The state of a deployment
 */
export class SubscriptionState {
    @ApiProperty({
        description: 'State of the Subscription',
        enum: SubscriptionStates,
    })
        state: string;

    @ApiProperty({
        description: 'The Account ID of the subscriber / Subscription ID',
    })
        subscriberAccountId: string;

    @ApiProperty({
        description: 'Timestamp of the last update',
    })
        lastUpdate: number;
}

export class SubscriptionDetail extends SubscriptionState {

    @ApiProperty({
        description: 'Deployment Type, the descriptor must conform to',
    })
        deploymentType: string;

    @ApiProperty({
        description: 'Deployment descriptor as JSON string',
    })
        deploymentDescriptor: string;

}