// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import {
    CanActivate,
    ExecutionContext,
    Injectable, Logger,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { ConfigService } from '@nestjs/config';
import { DEVELOPMENT_SECRET } from './ogate.module';


/**
 * Authenticates API requests.
 * Uses JWT token authentication, tokens are signed at the Reactor.
 */
@Injectable()
export class AuthGuard implements CanActivate {

    private readonly logger = new Logger(AuthGuard.name);

    constructor(private jwtService: JwtService, private config: ConfigService) {
        // check for Development secret
        // Warn the secret is very insecure
        if (config.get<string>('OGATE_SECRET_TOKEN') == DEVELOPMENT_SECRET) {
            this.logger.warn('Development secret token detect, DO NOT USE IN PRODUCTION');
            // lets generate some magic development tokens for UI auth during development
            ['ALICE', 'BOB', 'CHARLIE'].forEach((a=>{
                const token = this.jwtService.sign({ providerAccountId:a });
                this.logger.debug(`Dev token for ${a} is ${token} never expires`);
            }));
        }
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        if (!token) {
            throw new UnauthorizedException();
        }
        try {
            const payload = await this.jwtService.verifyAsync(
                token,
                {
                    secret:this.config.get<string>('OGATE_SECRET_TOKEN'),
                },
            );
            // 💡 Controllers must use the request object to access
            // the credentials
            request['user'] = payload;
        } catch {
            throw new UnauthorizedException();
        }
        return true;
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}