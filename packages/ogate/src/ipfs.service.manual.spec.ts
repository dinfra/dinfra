// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';
import { JsonSchemaService } from './json.schema.service';

/**
 * These unit tests require the IPFS content server to be running
 */
describe('IpfsService', () => {
    let service: IpfsService;
    let schema: JsonSchemaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [HttpModule, ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_IPFS_GATEWAY: Joi.string().default('http://localhost:8080'),
                    OGATE_CID_MAX_SIZE_KB: Joi.number().default(1024),
                }),
            }),
            ],
            providers: [IpfsService, JsonSchemaService],
        }).compile();
        service = module.get<IpfsService>(IpfsService);
        schema = module.get<JsonSchemaService>(JsonSchemaService);

    });

    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(schema).toBeDefined();
    });


    it('Can Validate Sample JSON Deployment Descriptor with JSON Deployment Type', async () =>{
        const descriptor = await service.getObjectCid('QmW3MxFLToWEpsr5TvX7msYSMCe18BCirzWPPFLhKF7xqQ');
        await schema.ensureValid({ source:'ipfs', path:'QmSAMNg77t9PeJxyaVv2tqjwqKjibefhfrtbMgWxeUnjyJ' }, descriptor);
    });

    it('Can Validate Sample YAML Deployment Descriptor with JSON Deployment Type', async () =>{
        const descriptor = await service.getObjectCid('QmUNH3tMN1rbgmvTCMH6ZbgofkHokAoj688uwzkEzgQiRP');
        await schema.ensureValid({ source:'ipfs', path:'QmSAMNg77t9PeJxyaVv2tqjwqKjibefhfrtbMgWxeUnjyJ' }, descriptor);
    });

    it('Can Validate Sample JSON Deployment Descriptor with YAML Deployment Type', async () =>{
        const descriptor = await service.getObjectCid('QmW3MxFLToWEpsr5TvX7msYSMCe18BCirzWPPFLhKF7xqQ');
        await schema.ensureValid({ source:'ipfs', path:'QmQeGpsWuvsrN2a7VGesPpk6kAm4YKafiUrwDtRxgyppR6' }, descriptor);
    });

    it('Can Validate Sample YAML Deployment Descriptor with YAML Deployment Type', async () =>{
        const descriptor = await service.getObjectCid('QmUNH3tMN1rbgmvTCMH6ZbgofkHokAoj688uwzkEzgQiRP');
        await schema.ensureValid({ source:'ipfs', path:'QmQeGpsWuvsrN2a7VGesPpk6kAm4YKafiUrwDtRxgyppR6' }, descriptor);
    });

});
