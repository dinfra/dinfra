// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Controller, Get, HttpException, Logger, UseGuards, Request, Param, Inject } from '@nestjs/common';
import { RequestCancelledError } from './simulator.dinfra.service';
import { DinfraService } from './ogate.types';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiResponse, ApiTags, ApiParam } from '@nestjs/swagger';
import { AuthGuard } from './auth.guard';
import { Version, Event, SubscriptionState, SubscriptionDetail } from './ogate.types';

/**
 * This controller forwards API requests to the DINFRA service or the simulator.
 * This class must be simple, there should not be any logic here.
 *
 * Note that the API is documented mostly here, and also where the types are defined.
 */
@Controller()
@ApiTags('server')
export class OGateController {

    private readonly logger = new Logger(OGateController.name);
    private readonly tracing = false;

    constructor(@Inject('DINFRA_SERVICE') private readonly appService: DinfraService) {
        // init
    }

    /**
     * Returns the version of OGate
     */
    @Get('/version')
  @ApiOperation({ operationId:'get_version' })
  @ApiOkResponse({
      description: 'Returns DINFRA OGage Version',
      type: Version,
  })
    getVersion(): Version {
        return {
            version: this.appService.getVersion(),
        };
    }

    /**
     * Listens to the next DINFRA event.
     * This is the entrypoint to a busy wait push service.
     * Chain reactors are only allowed one session. However there can be multiple chain reactors.
     * In practice one chain reactor will cover one Cloud Zone.
     * @param req
     */
    @Get('/event')
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({ operationId: 'get_event' })
    @ApiOkResponse({
        description: 'Returns the next event for the calling provider or NoEvent if no event was received. Providers receive push notifications by running a busy wait event loop',
        type: Event,
    })
    @ApiResponse({
        status: 491,
        description: 'A request was sent while another was already pending.',
    })
    async getEvent(@Request() req):Promise<Event> {
        // TODO: ClientID will come from Authentication
        try{
            if (this.tracing) this.logger.debug(`Request received from ${req.user.providerAccountId}`);
            return (await this.appService.getEvent(req.user.providerAccountId));
        } catch(e) {
            if (e instanceof RequestCancelledError) {
                this.logger.warn(e.message);
                throw new RequestCancelledHttpException();
            } else {
                throw e;
            }
        }
    }

    /**
     * Returns the list of subscriptions for the calling chain reactor.
     * @param req
     */
    @Get('/subscriptions')
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({ operationId: 'get_subscriptions' })
    @ApiOkResponse({
        description: 'Returns the list of subscriptions',
        type: SubscriptionState,
        isArray: true,
    })
    async getSubscriptions(@Request() req): Promise<Array<SubscriptionState>> {
        return this.appService.getSubscriptionList(req.user.providerAccountId);
    }

    /**
     * Returns the detail of a Subscription by its Account ID
     * @param req
     */
    @Get('/subscription/:subscriberAccountId')
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({ operationId: 'get_subscription' })
    @ApiParam({
        description: 'The subscriber Account ID',
        name:'subscriberAccountId',
    })
    @ApiOkResponse({
        description: 'Returns the detail of a subscription',
        type: SubscriptionDetail,
    })
    @ApiResponse({
        status: 404,
        description: 'The Requested subscription does not exists',
    })
    @ApiResponse({
        status: 492,
        description: 'The subscription content descriptor could not be retrieved from IPFS',
    })
    @ApiResponse({
        status: 493,
        description: 'The subscription content descriptor does not comply with the deployment type',
    })
    async getSubscription(
        @Param('subscriberAccountId') subscriberAccountId,
        @Request() req,
    ): Promise<SubscriptionDetail> {
        const s = this.appService.getSubscription(req.user.providerAccountId, subscriberAccountId);
        if(!s) throw new RecordNotFoundException();
        else return s;
    }

}

export class RequestCancelledHttpException extends HttpException {
    constructor() {
        super('Pending Request Cancelled', 491);
    }
}

export class SubscriptionDescriptorMissing extends HttpException {
    constructor(id) {
        super(`The subscription ${id} content descriptor could not be retrieved from IPFS`, 492);
    }
}

export class SubscriptionDescriptorNotCompliant extends HttpException {
    constructor(id) {
        super(`The subscription ${id} content descriptor does not comply with the deployment type`, 493);
    }
}

export class RecordNotFoundException extends HttpException {
    constructor() {
        super('The requested record does not exists', 404);
    }
}

