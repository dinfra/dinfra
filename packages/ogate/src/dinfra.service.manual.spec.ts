// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { DinfraAPIService, DinfraParachainService } from './dinfra.service';
import { QueueService } from './queue.service';
import { JsonSchemaService } from './json.schema.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';

describe('DinfraService', () => {
    let service: DinfraParachainService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [HttpModule, ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_DINFRA_RPC_URL: Joi.string().default('ws://localhost:37741'),
                }),
            }),
            ],
            providers: [QueueService, JsonSchemaService, DinfraAPIService, DinfraParachainService, IpfsService],
        }).compile();
        service = module.get<DinfraParachainService>(DinfraParachainService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('Can retrieve a Subscription', async () => {
        const d = await service.getSubscription('ALICE', 'BOB');
        expect(d).toBeTruthy();
    });

    it('Can cancel a Subscription', async () => {
        const d = await service.cancelSubscription('ALICE', 'BOB');
        expect(d).toBeTruthy();
    });
    
});
