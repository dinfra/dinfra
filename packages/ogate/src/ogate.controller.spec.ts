// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { OGateController } from './ogate.controller';
import { DinfraSimulatorService } from './simulator.dinfra.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import { QueueService } from './queue.service';
import { JwtModule } from '@nestjs/jwt';
import { JsonSchemaService } from './json.schema.service';
import { IpfsService } from './ipfs.service';
import { HttpModule } from '@nestjs/axios';

/**
 * The controller is mostly declarative.
 * They are just an entrypoint into the Dinfra service of the simulator.
 * Not much testing is expected here.
 */
describe('OGate Controller', () => {
    let ogController: OGateController;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [HttpModule, ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    OGATE_POLLING_INTERVAL: Joi.number().default(3),
                }),
            }),
            JwtModule.registerAsync({
                imports: [ConfigModule],
                useFactory: async (config: ConfigService) => ({
                    global: true,
                    secret: config.get<string>('OGATE_SECRET_TOKEN'),
                }),
                inject: [ConfigService],
            }),
            ],
            controllers: [OGateController],
            providers: [IpfsService,
                {
                    provide: 'DINFRA_SERVICE',
                    useClass: DinfraSimulatorService,
                }, QueueService, JsonSchemaService,
            ],
        }).compile();

        ogController = app.get<OGateController>(OGateController);
    });

    describe('getVersion', () => {
        it('should return a version field', () => {
            expect(ogController.getVersion()).toHaveProperty('version');
        });
    });
});
