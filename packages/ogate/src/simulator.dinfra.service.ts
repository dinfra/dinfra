// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { Injectable, Logger, OnModuleDestroy } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { QueueService, NOTHING } from './queue.service';
import { JsonSchemaService, SchemaRef } from './json.schema.service';
import { SubscriptionDetail, SubscriptionState, DinfraService, Event, Events } from './ogate.types';

export { RequestCancelledError } from './queue.service';
import _ from 'underscore';
import * as deepExtend from 'deep-extend';

export const DINFRA_SIMULATOR_SCRIPT_SCHEMA: SchemaRef = { source: 'file', path: 'schemas/simulator.dinfra.schema.yml' };

/**
 * The Dinfra Simulator service does not connect to the DINFRA parachain
 * Instead it generates scripted events for the purpose of implementing
 * Chain Reactors.
 *
 * The simulator is very simple. It generates events based on a timed script, and it also tracks state.
 * The simulator does not really care if the even flow makes sense, or if the changes in state are logical.
 * Simulators scripts have to be created knowing which events would be natural, or which cases want to be tested.
 *
 */
@Injectable()
export class DinfraSimulatorService implements OnModuleDestroy, DinfraService {

    private sessions = {};
    private readonly simulator_script;
    private readonly logger = new Logger(DinfraSimulatorService.name);

    constructor(private readonly config: ConfigService, private readonly queue: QueueService, private readonly schemaService: JsonSchemaService) {
        const simulatorData: string = fs.readFileSync(config.get<string>('OGATE_SIMULATOR_SCRIPT'), 'utf8');
        this.simulator_script = yaml.load(simulatorData);
        // validated the script, note this is async
        schemaService.ensureValid(DINFRA_SIMULATOR_SCRIPT_SCHEMA, this.simulator_script);
        this.logger.log('Simulator scripts loaded and validated.');
    }

    public getVersion(): string {
        return this.config.get('npm_package_version');
    }

    /**
     * Returns the next event for a given provider account / reactor.
     * @param providerAccountId the requesting reactor.
     */
    public async getEvent(providerAccountId):Promise<Event> {
        this.checkSession(providerAccountId);
        let e = await this.queue.getFirst(providerAccountId);
        // It converts the queue NOTING into an API Event.
        if(e == NOTHING) {
            e = {
                type: Events.NoEvent,
                timestamp: this.now(),
            };
        }
        return e;
    }

    /**
     * Returns the Subscription lists for a provider account (calling reactor).
     * Note that the reactor must have asked for an event first, because the simulator loads the state on first event.
     *
     * @param providerAccountId
     */
    public async getSubscriptionList(providerAccountId): Promise<Array<SubscriptionState>> {
        const session = this.sessions[providerAccountId];
        if (session) return session.getDeploymentList();
    }

    /**
     * returns the subscription detail by id
     * @param providerAccountId the calling chain reactor
     * @param subscriptionAccountId the deployment id to retrieve
     */
    public async getSubscription(providerAccountId, subscriptionAccountId): Promise<SubscriptionDetail> {
        const session = this.sessions[providerAccountId];
        if (session) return session.getSubscription(subscriptionAccountId);
    }


    /**
     * Returns a Unix epoch timestamp for events.
     * @private
     */
    private now() {
        return Math.floor(Date.now() / 1000);
    }

    /**
     * Chech if the session for an account (calling reactor) has or not been started.
     * @param providerAccountId the calling reactor
     * @private
     */
    private checkSession(providerAccountId) {
        let create = false;
        const session = this.sessions[providerAccountId];
        if(!session) create = true;
        else if (session.isFinalized()) create = true;

        if(create) this.sessions[providerAccountId] = new SimulatedDinfraSession(providerAccountId, this.simulator_script, this.queue);
    }

    public getSimulationScript() {
        return this.simulator_script;
    }

    /**
     * Clean up all the simulator sessions for accounts
     */
    onModuleDestroy(): any {
        const providerAccountIds = Object.keys(this.sessions);
        providerAccountIds.forEach(c => {
            this.sessions[c].destroy();
        });
    }
}


/**
 * This is a high level simulation of an RPC session with DINFRA network.
 * It basically generates high level events that would have originated in the parachain
 * Scripts are essentially a list of events indexed by relative time in seconds.
 * This class queues events according to an internal clock.
 */

class SimulatedDinfraSession {
    private finalized = false;
    private simulation: DinfraSimulation;
    private second: number = 0;
    private intervalId;
    private readonly logger = new Logger(SimulatedDinfraSession.name);

    constructor(private readonly providerAccountId, private readonly simulator_script, private readonly queue: QueueService) {
        this.simulation = new DinfraSimulation(simulator_script, providerAccountId);
        // Starts the clock
        this.intervalId = setInterval(()=>{
            this.second++;
            this.tick(this.second);
        }, 1000);
    }

    /**
     * this is the main clock function, it ticks in integer seconds.
     * @param second
     * @private
     */
    private tick(second) {
        const event = this.simulation.getEvent(second);
        if(event) {
            this.logger.debug(`${this.providerAccountId} simulation: triggering ${event.type} for customer at ${second}s`);
            this.queue.addLast(this.providerAccountId, event);
            if (event.type == Events.SimulationFinished) this.finalizeSimulation();
        }
    }

    private getSubscriptionList() {
        return this.simulation.getSubscriptionList();
    }

    private getSubscription(subscriberAccountId) {
        return this.simulation.getSubscription(subscriberAccountId);
    }


    /**
     * Stops the simulation as its last event. Events wont be generated again unless the account requests another event.
     * @private
     */
    private finalizeSimulation() {
        this.finalized = true;
        clearInterval(this.intervalId);
    }

    /**
     * Returns if the simulation has finished or not.
     */
    public isFinalized() {
        return this.finalized;
    }

    public destroy() {
        this.finalizeSimulation();
    }


}

/**
 * Utility class to help navigate the simulation script, the actual YAML file.
 * It adds typing while returning and detects when an event is also updating state.
 *
 * It also add meta-events for start and stop the simulation. This events exist only in the simulator and not
 * in the real service, but they are useful to run QA tests on the reactors, so that hey know when the test started
 * and finished, note that reactors sessions are basically and infinite loop.
 */
export class DinfraSimulation {
    private readonly logger = new Logger(DinfraSimulation.name);
    private steps;
    private subscriptions = {};
    constructor(private readonly simulator_script, private readonly providerAccountId) {
        // nothing
        const myScript = _.findWhere(simulator_script.simulatorScript, { providerAccountId: providerAccountId });
        this.steps = myScript.simulationSteps;
        // index our deployments by ID...assume just updated... just for fun.
        myScript.initialState.forEach((d)=>{
            d.lastUpdate = this.now();
            this.subscriptions[d.subscriberAccountId] = d;
        });
    }

    /**
     * returns an event for the given second, if any
      */
    public getEvent(second):Event {
        const step = _.findWhere(this.steps, { timeSeconds: second });
        if(step) {
            if(second == 1) this.logger.warn('First second is reserved for Simulation Started Event');
            // Add some mssing thigns to the event, like the timestamp and check if the event
            // also updates the state.
            step.subscriptionEvent.timestamp = this.now();
            this.updateState(step.subscriptionEvent);
            return {
                type: step.subscriptionEvent.eventType,
                subscriberAccountId: step.subscriptionEvent.subscriberAccountId,
                timestamp: step.subscriptionEvent.timestamp,
            };
        } else if (second == 1) {
            return {
                type: Events.SimulationStarted,
                timestamp: this.now(),
            };
        } else if (second > this.getDuration()) {
            return {
                type: Events.SimulationFinished,
                timestamp: this.now(),
            };
        }
    }

    /**
     * check if the event updates state, do if required
     */
    private updateState(event) {
        if (event.subscriptionStateUpdate) {
            const currentState = this.subscriptions[event.subscriberAccountId];
            if(!currentState) throw new Error(`Simulation Error: SubscriberAccountID ${event.subscriberAccountId} does not exist while processing event ${event.eventType} for ${this.providerAccountId}`);
            const stateUpdate = event.deploymentStateUpdate;
            // Will deep merge state Updates over current state
            deepExtend(currentState, stateUpdate);
            currentState.lastUpdate = this.now();
            this.subscriptions[event.subscriberAccountId] = currentState;
        }
    }

    /**
     * returns an array with the steps times in seconds
     */
    public getStepTimes() {
        return this.steps.map((s)=>s.timeSeconds);
    }

    /**
     * Get the list of deployments and their current state
     */
    public getSubscriptionList(): Array<SubscriptionState> {
        const subscriberAccountIds = Object.keys(this.subscriptions);
        return subscriberAccountIds.map(subscriberAccountId => {
            const subscription = this.subscriptions[subscriberAccountId];
            return {
                subscriberAccountId: subscriberAccountId,
                state: subscription.state,
                lastUpdate: subscription.lastUpdate,
            };
        });
    }

    public getSubscription(subscriberAccountId): SubscriptionDetail {
        const s = this.subscriptions[subscriberAccountId];
        return {
            subscriberAccountId: s.subscriberAccountId,
            state: s.subscriptionState,
            lastUpdate: s.lastUpdate,
            deploymentType: s.deploymentTypeId,
            deploymentDescriptor: JSON.stringify(s.deploymentDescriptor),
        };
    }

    /**
     * Returns the duration in seconds of the simulation
     */
    public getDuration() {
        const step_times = this.getStepTimes();
        return step_times[step_times.length - 1];
    }

    private now() {
        return Math.floor(Date.now() / 1000);
    }

}