# Deployment Descriptors

Deployment descriptors are JSON/YAML documents that describe infrastructure.

DINFRA takes a novel approach in which is does not interfere with the content of the descriptors. Allowing infrastructure 
providers and consumer to arrange deployment without limitations imposed by DINFRA as a mediator.

Descriptors however need to be properly specified, so that Infrastructure providers get orders that they understand and can 
fulfill. 

DINFRA uses [JSON Schema](http://json-schema.org/) validation to ensure that Providers receive descriptors that they understand
and can implement. 

DINFRA will allow providers to register types of deployments that they can serve. By associating a type of deployments with an
JSON Schema, DINFRA can ensure that providers get what they need without having to understand what it actually means. DINFRA 
can ensure that deployment descriptors are syntactically correct. 

Deployment descriptors are nothing new, when you write a `docker compose` or `helm chart` you are in fact doing the same thing.
They can be considered generic deployment descriptors created according to the capabilities of the platforms that will roll 
them out.

## Designing Deployment Descriptors

When integrating with DINFRA writing a chain reactor will be required, but also registering the deployment types that the 
infrastructure can serve. In some cases generic deployment descriptors can be used, in other cases is better to use specific
descriptors according to specialize services. 

## Samples Of Deployment Descriptors

Deployment descriptors can be generic of specific depending on whether they serve a concreate use case or a not. They can 
also be standard or proprietary, depending on the creator.

For example: `docker compose` and `helm` can be considered generic and probably industry standard by now.

However, specialized infrastructure providers should consider creating their own descriptors according to the services they provide.

# Deployment descriptors in the Substrate ecosystem.

The substrate ecosystem is plenty of specialized infrastructure providers. The following examples attempt to inspire the 
community in terms of which deployment descriptors could be provided.

## IPFS Content Sample Service

Many DAPPs providers depend on application content to be delivered over IPFS, a deployment descriptor could be created for the
purpose, with, perhaps the following structure:

```yaml
ipfs_service:
  pin_set:
    - name: MyDAPPUI
      CID: QmZbBe6qKdHufXnVYEKap6vae4DgfAhRwrzt69MAdqHK9k
    - name: MyAPPWebsite
      CID: QmateDTR5eabDoeEemMB2L6mFmEAZGB2eyNPf3gZPWHgcy
    - name: MyAPPDocumentation
      CID: QmPAycWnksXTKaz4DZYjC132EMEhH5GyUQoVfCezZCzyjP
  region_set:
    - CN
    - DE
    - SV
```

The previous example would allow the consumer to pass a collection of CIDs, identified, and the collection of regions for 
hosting. The provider would PIN those CIDs on the specified regions for while the deployment is funded.

A Draft JSON Schema for this descriptor would be [ipfs_service.schema.yml](./ipfs_service.schema.yml)

## RPC Nodes

Many Substrate Distributed applications connect to Substrate blockchains via RPC nodes. Other services like wallets or indexers
require RPC nodes too. Blockchain teams tend to ensure that there are enough public quality endpoints while stablished services
may comission their own to assure best possible quality.

A specialized deployment descriptor to serve nodes could look like:

```yaml
rpc_service:
  - chain: acala
    node_type: full
    instances: 5
  - chain: acala
    node_type: archive
  - chain: karura
    node_type: full
    instances: 3    
  - chain: karura
    node_type: archive
```

In this example of parachain team would be supposedly requesting node clusters for their main and canary chains
with a mix of both full nodes and archive nodes. 

A Draft JSON Schema for this descriptor would be [node_service.schema.yml](./node_service.schema.yml)

## Substrate Test Network

Many Substrate development need to launch a test network with several nodes, and ecosystems tools exist for the purpose.
The following deployment descriptor will host a test network using the tool [parachian-launch](https://github.com/open-web3-stack/parachain-launch/tree/master).
It will embed a configuration file of the tool and allow for some cluster settings.

```yaml
test_network:
    cluster:
        instance_size: large
        instances: 3
    parachain_launch:
        version: v1.4.0
        configuration:
            # parachain-launch config example copied here
            relaychain:
              image: parity/polkadot:v0.9.31
              chain: rococo-local
              runtimeGenesisConfig:
                configuration:
                  config:
                    validation_upgrade_cooldown: 10
                    validation_upgrade_delay: 10
              env:
                RUST_LOG: parachain::candidate-backing=trace,parachain::candidate-selection=trace,parachain::pvf=debug,parachain::provisioner=trace
              flags:
                - --rpc-methods=unsafe
                - --wasm-execution=compiled
                - --execution=wasm
              nodes:
                - name: alice
                - name: bob
            
            parachains:
            - image: acala/acala-node:2.11.0
              chain:
                base: acala-local
                collators:
                  - alice
                sudo: alice
              id: 2000
              parachain: true
              flags:
                - --rpc-methods=unsafe
                - --force-authoring
                - --wasm-execution=compiled
                - --execution=wasm
              relaychainFlags:
                - --wasm-execution=compiled
                - --execution=wasm
              env:
                RUST_LOG: sc_basic_authorship=trace,cumulus-consensus=trace,cumulus-collator=trace,collator_protocol=trace,collation_generation=trace,aura=debug
              volumePath: /acala/data
              nodes:
              - flags:
                - --alice            
```


A Draft JSON Schema for this descriptor would be [test_network.schema.yml](./test_network.schema.yml)

## Toolset

JSON Schema is widely used and therefore provides a number of libraries and tools that can be easily integrated.

For example, a [JSON Editor](https://github.com/json-editor/json-editor) aware of JSON Schema will automatically generate forms to capture a deployment desciptor.

Here is the populated form generated for schema and content of the RPC example above:

![RPC Sample Editor](./JSON Editor Interactive Example RPC.png)

A similar example for the IPFS service can be found [here](./JSON%20Editor%20Interactive%20Example%20IPFS.png).

## Summary

Deployment descriptors are simple in concept and easy to specify. Infrastructure providers should create their own to ensure they
get the information they need from their consumers. 

Deployment descriptors are no different from a menu in a restaurant, consumers need to be know what the possibilities are before making their orders.

JSON Schema provides plenty of options for creating rich examples. JSON Schema implementations include many extensions for, for example
performing validations on the provided parameters. 