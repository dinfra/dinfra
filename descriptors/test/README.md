# Test Development Data

The files on this directory are automatically added to the local IPFS network for testing.

## Deployment Schemas

They represent a type of deployment. Infrastructure providers need to register their capability to serve the given type/schema.

The deployment type schemas above are available for testing. 

## Deployment Descriptors

Deployment descriptors represent concrete requests for infrastructure and must conform with the deployment type.

The deployment descriptors above are available for testing.

## Adding your own 

You can add your own descriptors or schemas to this directory, they will be automatically added to the IPFS network on start, 
they can also be added or updated with `yarn docker:ipfs:testdata`