# Composable Zombienet

This Docker packs everytying requried to run a zombienet inside a container used as part of a docker-compose
with the rest of the DINFRA components

The image contains both [zombienet](https://github.com/paritytech/zombienet) and the different polkadot binaries 
required to run the Substrate test network.

Additional binaries (such as DINFRA node binary) can be mounted when lauching the image and used in the configuration

# Spawn or test

The image will, by default, spawn the network in `config.yam`, if there are `zndsl` test files in the test direction will 
run each of them as zombienet tests.

Tests are expected to reference the `config.yaml`

# Stack

[Zombienet](https://github.com/paritytech/zombienet) is a command line tool to easily spawn ephemeral Polkadot/Substrate networks and perform tests against them

